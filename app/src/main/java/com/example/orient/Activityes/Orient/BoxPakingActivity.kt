package com.example.orient.Activityes.Orient

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.media.MediaPlayer
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.os.StrictMode
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.widget.doAfterTextChanged
import com.example.orient.OrientAdapter.PackageAdapter
import com.example.orient.OrientDataModel.*
import com.example.orient.R
import com.example.orient.SessionManager
import kotlinx.android.synthetic.main.activity_box_paking.*
import kotlinx.android.synthetic.main.activity_dashbord.*
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_seetings.*
import java.io.DataOutputStream
import java.io.IOException
import java.io.PrintWriter
import java.net.Socket
import java.sql.Connection
import java.sql.DriverManager
import java.text.SimpleDateFormat
import java.util.*


class BoxPakingActivity : LoaderActivity(),PackageAdapter.OnItemClickListener {
    val USERNAME = "USERNNAME"
    val SERVER_IP = "SERVER_IP"
    val SERVER_PASSWORD = "SERVER_PASSWORD"
    val SERVER_NAME = "SERVERNAME"
    val DATABASE_NAME = "DATABASE"
    val LINENUMBER = "LINENUMBER"
    val PRINTER_IP = "PRINTER_IP"
    val PRINTER_PORT = "PRINTER_PORT"
    val SHARED_PREFS = "shared_prefs"
    var con: Connection? = null
    var sharedpreferences: SharedPreferences? = null
    private var sessionManager: SessionManager? = null
    var list: List<*>? = null
    var arrayAdapter: ArrayAdapter<String>? = null
    var SERVER_IP_DA = ""
    var SERVER_PASSWORD_DA = ""
    var SERVER_NAME_DA = ""
    var DATABASE_NAME_DA = ""
    var packSize=0
    var enaBarcode = ""
    var clickCount = 0

    @RequiresApi(Build.VERSION_CODES.M)
    @SuppressLint("ResourceType")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_box_paking)

        list = ArrayList<String>()
        arrayAdapter = ArrayAdapter<String>(applicationContext, R.layout.box_packing_item_list, R.id.barcode_txt, list as ArrayList<String>)
        boxPacking1_listView!!.adapter = arrayAdapter

        boxPackingArrowBack_img.setOnClickListener {
            onBackPressed()
        }

        sessionManager = SessionManager(this@BoxPakingActivity)
        sharedpreferences = getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE)
        SERVER_IP_DA = sharedpreferences?.getString(SERVER_IP,"").toString()
        SERVER_PASSWORD_DA = sharedpreferences?.getString(SERVER_PASSWORD,"").toString()
        SERVER_NAME_DA = sharedpreferences?.getString(SERVER_NAME,"").toString()
        DATABASE_NAME_DA = sharedpreferences?.getString(DATABASE_NAME,"").toString()

        ipAndPort_txt.setText(sharedpreferences?.getString(PRINTER_IP,"").toString()+":"+sharedpreferences?.getString(PRINTER_PORT,"").toString())

        printeModel.printerIp = sharedpreferences?.getString(PRINTER_IP,"").toString()
        printeModel.printerPort = sharedpreferences?.getString(PRINTER_PORT,"").toString()


//        boxScan_editText.setOnKeyListener( View.OnKeyListener {v, keyCode, event ->
//            if (keyCode == KeyEvent.KEYCODE_ENTER ) {
//                val b: String = boxScan_editText.getText().toString()
//                if (b.equals("..")){
//                    (list as ArrayList<String>)?.clear()
//                    partNumber_txt.text = "XXXXXXXXXXXXXX"
//                    modelNumber_txt.text = "XXXXXXXXXXXXXX"
//                    colour_txt.text = "XXXXXXXXXXXXXX"
//                    totalPacking_txt.text = "Total: 0"
//                    requiredNumberValue.text = ""
//                    required_number_txt.text = ""
//                }
//                if (b.length > 10)
//                {
////                    val listData = b
//                    if (list!!.contains(b)) {
//                        Toast.makeText(this,"Already added$b", Toast.LENGTH_LONG).show()
//                        playSound(R.raw.error)
//                    }
//                    else
//                    {
//                        validateBarcode(b)
//                    }
//                } else {
//                    // Toast.makeText(this,"Enter the number of 12 digits", Toast.LENGTH_LONG).show()
//                }
//
//                boxScan_editText.getText()?.clear()
//                boxScan_editText.setFocusable(true)
//                boxScan_editText.requestFocus()
//                return@OnKeyListener true
//            }
//            false
//        })

           // GetPrnData()

        boxScan_editText.doAfterTextChanged{
            val b = boxScan_editText.text.toString().trim()
            if (b.equals("..")){
                (list as ArrayList<String>)?.clear()
                partNumber_txt.text = "XXXXXXXXXXXXXX"
                modelNumber_txt.text = "XXXXXXXXXXXXXX"
                colour_txt.text = "XXXXXXXXXXXXXX"
                totalPacking_txt.text = "Total: 0"
                requiredNumberValue.text = ""
                required_number_txt.text = ""
            }
            else if (b.length > 10){
                if (list!!.contains(b)) {
                    Toast.makeText(this,"Already added$b", Toast.LENGTH_LONG).show()
                    playSound(R.raw.error)
                }
                else
                {
                    validateBarcode(b)
                }
            }else{

            }
            boxScan_editText.getText()?.clear()
            boxScan_editText.setFocusable(true)
            boxScan_editText.requestFocus()

        }

    }

    private fun GetPrnData(packSize: String){
        try {
            con = connectionClass(SERVER_NAME_DA ,SERVER_PASSWORD_DA, DATABASE_NAME_DA, SERVER_IP_DA)
            if (con == null) {
                Toast.makeText(this, "Server Link Fail Try Again ....", Toast.LENGTH_LONG).show()
            } else {
                val sql = "select * From tabPrnDetails where PrnId='"+packSize+"'"
                val smt = con!!.createStatement()
                val data = smt.executeQuery(sql)
                if (data.next()) {
                    val prnformate = data.getString("Prn")
                    sessionManager?.SavePrn(prnformate)
                    printeModel.prn = sharedpreferences?.getString("PRN","").toString()
                } else {
                    Toast.makeText(this, "Prn Getting Fail....", Toast.LENGTH_LONG).show()
                }
            }
        }catch (e: Exception){
            Log.e("TAg",e.message!!)
        }
    }

    @SuppressLint("SuspiciousIndentation")
    private fun validateBarcode(barcode:String){
            try {
                con = connectionClass(SERVER_NAME_DA ,SERVER_PASSWORD_DA, DATABASE_NAME_DA, SERVER_IP_DA)
                if (con == null) {
                    Toast.makeText(this, "Server Link Fail Try Again ....", Toast.LENGTH_LONG).show()
                } else {
                    val sql = "Select * from VW_SERIALNO_INFO where EXISENO='"+barcode+"'"
                    val smt = con!!.createStatement()
                    val data = smt.executeQuery(sql)
                    if(data.next()) {
                        if(!data.getBoolean("Blocked"))
                        {
                            if(!data.getBoolean("Scanned"))
                            {
                                var listData=data.getString("EXISENO")
                               if(list!!.size<=0)
                               {
                                   requiredNumberValue.text = (list as ArrayList<String>).size.toString()
                                   partNumber_txt.text= data.getString("PoductCode")
                                   modelNumber_txt.text =  data.getString("Model")
                                   lastScanNo_txt.text = data.getString("EXISENO")
                                   colour_txt.text= data.getString("Color")+"/"+data.getString("Sweeps")
                                //data.getString("SAPPoductCode")
                                   packSize= data.getInt("PackSize")
                                   this.required_number_txt.text = "/"+packSize.toString()
                                   (list as ArrayList<String>).add(listData)
                                   boxPacking1_listView.setAdapter(arrayAdapter)
                                   requiredNumberValue.text = (list as ArrayList<String>).size.toString()
                                   arrayAdapter!!.notifyDataSetChanged()
                               }
                                else
                               {
                                   if(partNumber_txt.text.equals(data.getString("PoductCode")))
                                   {
                                       if(list!!.size<packSize)
                                       {
                                       (list as ArrayList<String>).add(listData)
                                           boxPacking1_listView.setAdapter(arrayAdapter)
                                       arrayAdapter!!.notifyDataSetChanged()
                                           requiredNumberValue.text = (list as ArrayList<String>).size.toString()
                                       }
                                       else
                                       {
                                           Toast.makeText(this, "Box Full No More Scan Accepted..", Toast.LENGTH_LONG).show()
                                           playSound(R.raw.error)
                                       }
                                   }
                               }
                                //check box full create box and save data
                                if(list!!.size>=packSize){
                                        PostData()
                                }
                            }
                            else
                            {
                                Toast.makeText(this, "Already Packed On Box :"+data.getString("BoxNo"), Toast.LENGTH_LONG).show()
                                lastScanNo_txt.text = data.getString("BoxNo")
                             //   alreadyPackBoxDialog(data.getString("BoxNo"))
                                playSound(R.raw.error)
                            }
                        }
                        else
                        {
                            Toast.makeText(this, "Blocked Serial No....", Toast.LENGTH_LONG).show()
                            playSound(R.raw.error)
                        }

                     }
                    else
                    {
                        Toast.makeText(this, "Invalid Serial No....", Toast.LENGTH_LONG).show()
                    }
                }
            }catch (e: Exception){
                Log.e("TAg",e.message!!)
            }
    }

private fun ean13CheckDigit(barcode: String): String? {
    var s = 0
    for (i in 1..barcode.length) {
        val c = Character.getNumericValue(barcode[i-1])
        s += c * if (i % 2 == 0) 1 else 3
    }
    s = (10 -(s % 10))

    enaBarcode = barcode+s
    return enaBarcode
}

    private fun getMfgMMYY(): String {
        try {
            var mmyy=""
            con = connectionClass(SERVER_NAME_DA ,SERVER_PASSWORD_DA, DATABASE_NAME_DA, SERVER_IP_DA)
            if (con == null) {
                Toast.makeText(this, "Server Link Fail Try Again ....", Toast.LENGTH_LONG).show()
            } else {
                val sql = "Select Mth=case Month(getdate()) " +
                        "when 1 then 'Jan' " +
                        "when 2 then 'Feb' " +
                        "when 3 then 'Mar' " +
                        "when 4 then 'Apr' " +
                        "when 5 then 'May' " +
                        "when 6 then 'Jun' " +
                        "when 7 then 'Jul' " +
                        "when 8 then 'Aug' " +
                        "when 9 then 'Sept' " +
                        "when 10 then 'Oct' " +
                        "when 11 then 'Nov' " +
                        "when 12 then 'Dec' End + ' - ' + convert(varchar,Year(getdate())) x"
                val smt = con!!.createStatement()
                val data = smt.executeQuery(sql)

                if(data.next()) {
                    mmyy = data.getString("x")
                }
                else
                {
                    mmyy=""
                }
            }
            return mmyy
        }catch (e: Exception){
            Log.e("TAg",e.message!!)
            return ""
        }
    }

    private fun PostData(){
        try {
            con = connectionClass(SERVER_NAME_DA ,SERVER_PASSWORD_DA, DATABASE_NAME_DA, SERVER_IP_DA)
            if (con == null) {
                Toast.makeText(this, "Server Link Fail Try Again ....", Toast.LENGTH_LONG).show()
            } else {
                var EAN = ean13CheckDigit(list!!.size.toString()+partNumber_txt.text.substring(1))
                var MfgmmYY=getMfgMMYY()
                val sql = "SP_INSERT_NEWBATCHINFO '10','"+partNumber_txt.text+"','"+partNumber_txt.text +"','"+list!!.size+"','"+EAN+"','"+MfgmmYY+"','"+sharedpreferences?.getString(LINENUMBER,"")+"','"+sharedpreferences?.getString(PRINTER_IP,"")+"','"+sharedpreferences?.getString(PRINTER_PORT,"")+"'"
                val smt = con!!.createStatement()
                val data = smt.executeQuery(sql)
                if(data.next()) {
                    GetPrnData(packSize.toString())
                    val boxno = data.getString("BatchNo").toString()
                    var seq=0;
                    for (i in list!!){
                        val fist = i
                        seq++;
                        val sqlInDetais = "INSERT INTO tblBatchDetail(BatchNo,ScanSeq,ScanDT,ExiseNo) VALUES("+boxno.toString()+","+seq.toString()+",getdate(),'"+fist.toString()+"')"
                        val sqlUpDetais = "update tblExise set Scanned=1,BoxNo='" + boxno+ "' where ExiseNo='" + fist.toString() +"'"
                        smt.execute(sqlInDetais)
                        smt.execute(sqlUpDetais)
                    }
                    Toast.makeText(this, "Box No.: "+boxno.toString()+" Created And Saved....", Toast.LENGTH_LONG).show()
                    PrintPacketLabel(boxno)
                }
                else
                {
                    Toast.makeText(this, "Invalid Serial No....", Toast.LENGTH_LONG).show()
                }
            }
        }catch (e: Exception){
            Log.e("TAg",e.message!!)
        }
    }

    private fun PrintPacketLabel(boxNo :String){
        try {
            con = connectionClass(SERVER_NAME_DA ,SERVER_PASSWORD_DA, DATABASE_NAME_DA, SERVER_IP_DA)
            if (con == null) {
                Toast.makeText(this, "Server Link Fail Try Again ....", Toast.LENGTH_LONG).show()
            } else {
                var prn = printeModel.prn
                val sql = "Select * from VW_BATCH_DETAIL where BatchNo='"+boxNo+"'"
                val smt = con!!.createStatement()
                val data = smt.executeQuery(sql)
                var row : Int = 0
                while (data.next()) {
                    row=row+1
                    for(c in 1 until  data.getMetaData().getColumnCount()){
                        prn=prn.replace("<FE/>"+data.getMetaData().getColumnName(c)+"</FE>",data.getString(c).toString(),true)
                        prn=prn.replace("<DE"+row.toString()+"/>"+data.getMetaData().getColumnName(c)+"</DE>",data.getString(c).toString(),true)
                    }
                }
                val messageSender = MessageSender()
                    messageSender.execute(prn)

                (list as ArrayList<String>).clear()
                partNumber_txt.text = "XXXXXXXXXXXXXX"
                modelNumber_txt.text = "XXXXXXXXXXXXXX"
                colour_txt.text = "XXXXXXXXXXXXXX"
                requiredNumberValue.text = ""
                required_number_txt.text = ""
                clickCount++
                totalPacking_txt.text = "Box Packed: "+clickCount.toString()
                lastScanNo_txt.text = boxNo

            }
        }catch (e: Exception){
            Log.e("TAg",e.message!!)
        }
    }

    class MessageSender : AsyncTask<String?, Void?, Void?>() {
        var s: Socket? = null
        var pw: PrintWriter? = null
        var dos: DataOutputStream? = null
        protected override fun doInBackground(vararg voids: String?): Void? {
            val message = voids[0]
            try {
                s = Socket(printeModel.printerIp,printeModel.printerPort.toInt())
                pw = PrintWriter(s!!.getOutputStream())
                pw!!.write(message)
                pw!!.flush()
                pw!!.close()
                s!!.close()
            } catch (ex: IOException) {
                ex.printStackTrace()
            }
            return null
        }
    }

    fun connectionClass(user: String, password: String, database: String, server: String): Connection? {
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        var connection: Connection? = null
        var connectionURL: String? = null
        try {
            Class.forName("net.sourceforge.jtds.jdbc.Driver")
            connectionURL = "jdbc:jtds:sqlserver://$server/$database;user=$user;password=$password;"
            connection = DriverManager.getConnection(connectionURL)
        } catch (e: Exception) {
            Log.e("SQL Connection Error : ", e.message!!)
        }
        return connection
    }

    override fun viewClickEvent(boxPackingModel: BoxPackingModel) {
        Toast.makeText(this,"Item Click"+boxPackingModel.barcode,Toast.LENGTH_LONG).show()
    }

    private fun playSound(resId: Int) {
        val mp = MediaPlayer.create(this, resId)
        mp.setOnCompletionListener { mediaPlayer ->
            mediaPlayer.reset()
            mediaPlayer.release()
        }
        mp.start()
    }


    private fun alreadyPackBoxDialog(boxNumber: String){
        val dialog = Dialog(this,R.style.ThemeOverlay_MaterialComponents_MaterialAlertDialog_Background)
        dialog.setContentView(R.layout.custom_dialog1)
        val yesBtn = dialog.findViewById<Button>(R.id.yes1_btn)
        val noBtn = dialog.findViewById<Button>(R.id.no1_btn)
        yesBtn.setOnClickListener {
            PrintPacketLabel(boxNumber)
            dialog.dismiss()
        }
        noBtn.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }
}


