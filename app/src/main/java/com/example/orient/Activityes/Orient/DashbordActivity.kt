package com.example.orient.Activityes.Orient

import android.R.string
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.orient.Activityes.PointCheck.SecurityCheckActivity
import com.example.orient.Activityes.PointCheck.SelectShiftActivity
import com.example.orient.Activityes.PointCheck.TSDispatchActivity
import com.example.orient.OrientDataModel.UserData
import com.example.orient.R
import com.example.orient.SessionManager
import kotlinx.android.synthetic.main.activity_dashbord.*


class DashbordActivity : AppCompatActivity() {
    private var pressedTime: Long = 0
    val USERNAME = "USERNNAME"
    val SHARED_PREFS = "shared_prefs"
    var sharedpreferences: SharedPreferences? = null
    private var sessionManager: SessionManager? = null
    private var f:String = ""
    private var l:String = ""
    private var t:String = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashbord)
        sessionManager = SessionManager(this@DashbordActivity)
        sharedpreferences = getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE)
        userName_txt.text = sharedpreferences?.getString(USERNAME,"").toString()
        val adminName = sharedpreferences?.getString(USERNAME,"").toString()
        tstechUserName_txt.text = sharedpreferences?.getString(USERNAME,"").toString()

        UpdateProfile(adminName)

        logout_cardview.setOnClickListener {
            onBackPressed()
        }

        box_packing_cardview.setOnClickListener {
            startActivity(Intent(this, BoxPakingActivity::class.java))
        }
        Dispatch_layout.setOnClickListener {
            startActivity(Intent(this, DispatchActivity::class.java))

        }
        warrantlabel_layout.setOnClickListener {
            startActivity(Intent(this, WarrantLabelActivity::class.java))
        }


    }

    override fun onBackPressed() {
        if (pressedTime + 2000 > System.currentTimeMillis()) {
            super.onBackPressed()
            finish()
        } else {
            Toast.makeText(baseContext, "Press back again to exit", Toast.LENGTH_SHORT).show()
        }
        pressedTime = System.currentTimeMillis()
    }

    private  fun UpdateProfile(data:String){
        val nameArray = data.split(' ').toTypedArray()
        for ( i in nameArray.indices){
            if (i == 0){
                    val firstName = nameArray[0]
                    val firstLetter = firstName.split("")
                    f = firstLetter[1]
            }
            else if(i == 1){
                    val firstName = nameArray[1]
                    val firstLetter = firstName.split("")
                    l = firstLetter[1]

            }else if (i == 2){
                val firstName = nameArray[2]
                val firstLetter = firstName.split("")
                t = firstLetter[2]
            }
        }
        UserData.userName = f+l+t

    }

}