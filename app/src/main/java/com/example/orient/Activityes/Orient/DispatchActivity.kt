package com.example.orient.Activityes.Orient

import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.os.StrictMode
import android.text.Editable
import android.util.Log
import android.view.View
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.addTextChangedListener
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.orient.Adapter.OldItemAdapter
import com.example.orient.Dispatch.OldItemModel
import com.example.orient.OrientDataModel.OldItem
import com.example.orient.R
import com.example.orient.SessionManager
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_dispatch.*
import java.sql.Connection
import java.sql.DriverManager
import java.text.SimpleDateFormat
import java.util.*


class DispatchActivity : AppCompatActivity(), OldItemAdapter.OnItemClickListener {
    val USERNAME = "USERNNAME"
    val SERVER_IP = "SERVER_IP"
    val SERVER_PASSWORD = "SERVER_PASSWORD"
    val SERVER_NAME = "SERVERNAME"
    val DATABASE_NAME = "DATABASE"
    val LINENUMBER = "LINENUMBER"
    val PRINTER_IP = "PRINTER_IP"
    val PRINTER_PORT = "PRINTER_PORT"
    val PRN  = "PRN"
    val SHARED_PREFS = "shared_prefs"
    var cal = Calendar.getInstance()
    var con: Connection? = null
    var sharedpreferences: SharedPreferences? = null
    private var sessionManager: SessionManager? = null
    lateinit var oldItemAdapter: OldItemAdapter
    var SERVER_IP_DA = ""
    var SERVER_PASSWORD_DA = ""
    var SERVER_NAME_DA = ""
    var DATABASE_NAME_DA = ""
    private var OldItemList = ArrayList<OldItemModel>()
    lateinit var bottomNav : BottomNavigationView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dispatch)

        sessionManager = SessionManager(this@DispatchActivity)
        sharedpreferences = getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE)
        dispatch_userName.text = sharedpreferences?.getString(USERNAME,"").toString()
        SERVER_IP_DA = sharedpreferences?.getString(SERVER_IP,"").toString()
        SERVER_PASSWORD_DA = sharedpreferences?.getString(SERVER_PASSWORD,"").toString()
        SERVER_NAME_DA = sharedpreferences?.getString(SERVER_NAME,"").toString()
        DATABASE_NAME_DA = sharedpreferences?.getString(DATABASE_NAME,"").toString()

        dispatchArrowBack_img.setOnClickListener {
            onBackPressed()
        }

        val simpleDate = SimpleDateFormat("dd/M/yyyy")
        val currentDate = simpleDate.format(Date())
        println(" Current Date is: " +currentDate)
       // dispatchUserName.text = UserData.userName

        newLoading_btn.setOnClickListener {
            val intent = Intent(this,NewScanActivity::class.java)
            OldItem.issueNo = ""
            OldItem.date = ""
            startActivity(intent)
        }

        swipeRefreshLayout.setOnRefreshListener {

            swipeRefreshLayout.isRefreshing = false

           getOldBox()
        }


        getOldBox()

        searchView.addTextChangedListener { charSequence  ->
            try {
                val filter: ArrayList<OldItemModel> = ArrayList()
                if (OldItemList.size != 0){
                    OldItemList.forEach {
                        if (charSequence!= null){
                            if (it.DDate!!.contains(charSequence) || it.IssueNo!!.contains(charSequence)){
                                filter.add(it)
                            }
                        }
                        showOldList(filter)
                    }
                }

            } catch (error: Throwable) {
                Log.e("TAg",error.message!!)
            }
        }

        val dateSetListener = object : DatePickerDialog.OnDateSetListener {
            override fun onDateSet(view: DatePicker, year: Int, monthOfYear: Int,
                                   dayOfMonth: Int) {
                cal.set(Calendar.YEAR, year)
                cal.set(Calendar.MONTH, monthOfYear)
                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                updateDateInView()
            }
        }

        calender_img!!.setOnClickListener(object : View.OnClickListener {
            override fun onClick(view: View) {
                DatePickerDialog(this@DispatchActivity,
                    dateSetListener,
                    // set DatePickerDialog to point to today's date when it loads up
                    cal.get(Calendar.YEAR),
                    cal.get(Calendar.MONTH),
                    cal.get(Calendar.DAY_OF_MONTH)).show()
            }

        })

    }

    private fun updateDateInView() {
        val myFormat = "dd/MM/yyyy" // mention the format you need
        val sdf = SimpleDateFormat(myFormat, Locale.US)
        searchView.setText(sdf.format(cal.getTime()))
    }


    private fun getOldBox(){
        try {
            con = connectionClass(SERVER_NAME_DA ,SERVER_PASSWORD_DA, DATABASE_NAME_DA, SERVER_IP_DA)
            if (con == null) {
                Toast.makeText(this, "Server Link Fail Try Again ....", Toast.LENGTH_LONG).show()
            } else {
                val sql = "SELECT IssueNo,convert(varchar,DispatchDate,103) as DDate,VehicleNo FROM tblDespetch where isnull(Closed,0)=0"
                val smt = con!!.createStatement()
                val data = smt.executeQuery(sql)
                var row  = data.row
                OldItemList.clear()
                while (data.next()) {
                    row=row+1
                    var b = data.row
                        val issue = data.getString("IssueNo")
                        val date = data.getString("DDate")
                        val oldItemModel = OldItemModel(issue,date,"")
                        OldItemList.add(oldItemModel)

                    showOldList(OldItemList)
                }
            }

        }catch (e: Exception){
            Log.e("TAg",e.message!!)
        }
    }

    private fun showOldList(OldItemList: List<OldItemModel>){
         oldItemAdapter = OldItemAdapter(OldItemList,this)
        val layoutManage = LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false)
        oldItem_recyclerview.layoutManager = layoutManage
        oldItem_recyclerview.adapter = oldItemAdapter
    }


    fun connectionClass(
        user: String,
        password: String,
        database: String,
        server: String
    ): Connection? {
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        var connection: Connection? = null
        var connectionURL: String? = null
        try {
            Class.forName("net.sourceforge.jtds.jdbc.Driver")
            connectionURL = "jdbc:jtds:sqlserver://$server/$database;user=$user;password=$password;"
            connection = DriverManager.getConnection(connectionURL)
        } catch (e: Exception) {
            Log.e("SQL Connection Error : ", e.message!!)
        }
        return connection
    }

    override fun onItemClick(oldItemModel: OldItemModel) {
        addAlertDialog(oldItemModel.IssueNo.toString(),oldItemModel.DDate.toString(),oldItemModel.VehicleNo.toString())
    }

    private fun addAlertDialog(IssueNo:String,date:String,VehicleNo:String){
        val dialog = Dialog(this,R.style.ThemeOverlay_MaterialComponents_MaterialAlertDialog_Background)
        dialog.setContentView(R.layout.custom_dialog)
        val yesBtn = dialog.findViewById<Button>(R.id.yes_btn)
        val noBtn = dialog.findViewById<Button>(R.id.no_btn)
        val ussueNo = dialog.findViewById<TextView>(R.id.issue_no_text)
        ussueNo.text = IssueNo
        yesBtn.setOnClickListener {
            val intent = Intent(this,NewScanActivity::class.java)
            OldItem.issueNo = IssueNo
            OldItem.date = date
            startActivity(intent)
            dialog.dismiss()
        }

        noBtn.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }

    override fun onResume() {
        super.onResume()
        getOldBox()
    }

    override fun onRestart() {
        super.onRestart()
        getOldBox()
    }

}


