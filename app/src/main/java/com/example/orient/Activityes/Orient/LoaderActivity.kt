package com.example.orient.Activityes.Orient

import android.content.Intent
import android.view.LayoutInflater
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.example.orient.R

open class LoaderActivity: AppCompatActivity() {

    private lateinit var dialog: AlertDialog
    val intentFlag = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK

    fun showLoaderActivity() {
        val builder= AlertDialog.Builder(this)
        val dialogView= LayoutInflater.from(this).inflate(R.layout.progress_bar_layout,null)
        builder.setView(dialogView)
        builder.setCancelable(false)
        dialog = builder.create()
        dialog.show()
    }

    fun dismissLoaderActivity() {
        if (this::dialog.isInitialized){
            dialog.dismiss()
        }
    }

}