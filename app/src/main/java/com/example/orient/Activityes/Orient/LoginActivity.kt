package com.example.orient.Activityes.Orient

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.os.StrictMode
import android.os.StrictMode.ThreadPolicy
import android.util.Log
import android.widget.Toast
import com.example.orient.Activityes.PointCheck.TSDashbordActivity
import com.example.orient.SessionManager
import kotlinx.android.synthetic.main.activity_login.*
import java.sql.Connection
import java.sql.DriverManager
import java.sql.Statement

class LoginActivity : LoaderActivity() {

    val SHARED_PREFS = "shared_prefs"
    var sharedpreferences: SharedPreferences? = null
    private var sessionManager: SessionManager? = null
    var con: Connection? = null
    val IS_USER_LOGIN = "IsUserLoggedIn"
    val USERNAME = "USERNNAME"
    var stmt: Statement? = null
    private var serverIp = ""
    private var serverUsername:String = ""
    private var serverPass:String = ""
    private var databasename:String = ""
    private var lineNumber: String = ""
    private var printerIp: String = ""
    private var printPort:String = ""
    val SERVER_IP = "SERVER_IP"
    val SERVER_PASSWORD = "SERVER_PASSWORD"
    val SERVER_NAME = "SERVERNAME"
    val DATABASE_NAME = "DATABASE"
    val SERVER_STATUS = "CONNECT"

    var SERVER_IP_DA = ""
    var SERVER_PASSWORD_DA = ""
    var SERVER_NAME_DA = ""
    var DATABASE_NAME_DA = ""


    @SuppressLint("MissingInflatedId", "SuspiciousIndentation")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.example.orient.R.layout.activity_login)

        sessionManager = SessionManager(this@LoginActivity)
        sharedpreferences = getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE)
        SERVER_IP_DA = sharedpreferences?.getString(SERVER_IP,"").toString()
        SERVER_PASSWORD_DA = sharedpreferences?.getString(SERVER_PASSWORD,"").toString()
        SERVER_NAME_DA = sharedpreferences?.getString(SERVER_NAME,"").toString()
        DATABASE_NAME_DA = sharedpreferences?.getString(DATABASE_NAME,"").toString()
        if (sharedpreferences?.getString(SERVER_STATUS,null)!=null){
                Toast.makeText(this,"Server Connected"+SERVER_IP_DA,Toast.LENGTH_LONG).show()
        }else{
             startActivity(Intent(this, SeetingsActivity::class.java))
        }
        seeting_img.setOnClickListener {
            startActivity(Intent(this, SeetingsActivity::class.java))
        }

        button1.setOnClickListener {
            val u = username.text.toString().trim()
            if (u.isEmpty()){
                username.error = "Enter User Id"
            }else{
                Login()
            }
        }

    }

    private fun Login() {
        try {
            con = connectionClass( SERVER_NAME_DA,SERVER_PASSWORD_DA, DATABASE_NAME_DA, SERVER_IP_DA)
            if (con == null) {
                dismissLoaderActivity()
                Toast.makeText(this, "Server Link Fail Try Again ....", Toast.LENGTH_LONG).show()
            } else {
                val sql = "Select *  FROM tabUsers Where UserId='"+username.text.toString()+"' and Password='"+userpassword.text.toString()+"'"
                val smt = con!!.createStatement()
                val resultSet = smt.executeQuery(sql)
                if (resultSet.next()) {
                    dismissLoaderActivity()
                    val name = resultSet.getString("UserName")
                    sessionManager?.userDetails(name)
                            Security()
                    }else{
                        dismissLoaderActivity()
                        Toast.makeText(this,"Invalid"+" "+username.text.toString()+" "+userpassword.text.toString(),Toast.LENGTH_LONG).show()
                }
            }
    }catch (e: Exception){
            Log.e("TAg",e.message!!)
        }
    }

    private fun Security(){
        try {
            con = connectionClass( SERVER_NAME_DA, SERVER_PASSWORD_DA, DATABASE_NAME_DA, SERVER_IP_DA)
            if (con == null) {
                Toast.makeText(this, "Server Link Fail Try Again ....", Toast.LENGTH_LONG).show()
            } else {
                val sql = "select Year(getdate())"
                val smt = con!!.createStatement()
                val data = smt.executeQuery(sql)
                if (data.next()) {
                    val name = data.getString(1)
                    if (name.equals("2024")){
                        Toast.makeText(this,"Your Application has locked",Toast.LENGTH_LONG).show()
                    }else{
                        val intent = Intent(this, TSDashbordActivity::class.java)
                        startActivity(intent)
                        finish()
                    }

                }else{
                    Toast.makeText(this,"Invalid"+" "+username.text.toString()+" "+userpassword.text.toString(),Toast.LENGTH_LONG).show()
                }
            }
        }catch (e: Exception){
            Log.e("TAg",e.message!!)
        }
    }

    fun connectionClass(user: String, password: String, database: String, server: String): Connection? {
        val policy = ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        var connection: Connection? = null
        var connectionURL: String? = null

        try {
            Class.forName("net.sourceforge.jtds.jdbc.Driver")
            connectionURL = "jdbc:jtds:sqlserver://$server/$database;user=$user;password=$password;"
            connection = DriverManager.getConnection(connectionURL)
        } catch (e: Exception) {
            Log.e("SQL Connection Error : ", e.message!!)
        }
        return connection
    }

}