package com.example.orient.Activityes.Orient

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.media.MediaPlayer
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.StrictMode
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.core.widget.doAfterTextChanged
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.orient.Adapter.NewItemAdapter
import com.example.orient.Dispatch.NewItemModel
import com.example.orient.Dispatch.OldItemModel
import com.example.orient.OrientDataModel.OldItem
import com.example.orient.R
import com.example.orient.SessionManager
import kotlinx.android.synthetic.main.activity_box_paking.*
import kotlinx.android.synthetic.main.activity_new_scan.*
import java.sql.Connection
import java.sql.DriverManager
import java.util.ArrayList

class NewScanActivity : AppCompatActivity(),NewItemAdapter.OnItemClickListener {

    val USERNAME = "USERNNAME"
    val SERVER_IP = "SERVER_IP"
    val SERVER_PASSWORD = "SERVER_PASSWORD"
    val SERVER_NAME = "SERVERNAME"
    val DATABASE_NAME = "DATABASE"
    val LINENUMBER = "LINENUMBER"
    val PRINTER_IP = "PRINTER_IP"
    val PRINTER_PORT = "PRINTER_PORT"
    val PRN  = "PRN"
    val SHARED_PREFS = "shared_prefs"
    var con: Connection? = null
    var sharedpreferences: SharedPreferences? = null
    private var sessionManager: SessionManager? = null
    var SERVER_IP_DA = ""
    var SERVER_PASSWORD_DA = ""
    var SERVER_NAME_DA = ""
    var DATABASE_NAME_DA = ""
    private var newItemList = ArrayList<NewItemModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_scan)

        sessionManager = SessionManager(this@NewScanActivity)
        sharedpreferences = getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE)
        newBatchScan_userName.text = sharedpreferences?.getString(USERNAME,"").toString()
        SERVER_IP_DA = sharedpreferences?.getString(SERVER_IP,"").toString()
        SERVER_PASSWORD_DA = sharedpreferences?.getString(SERVER_PASSWORD,"").toString()
        SERVER_NAME_DA = sharedpreferences?.getString(SERVER_NAME,"").toString()
        DATABASE_NAME_DA = sharedpreferences?.getString(DATABASE_NAME,"").toString()

        unscanBox_btn.setOnClickListener {
            val intent = Intent(this,UnScanBoxActivity::class.java)
            startActivity(intent)
        }

        newScanArrowBack_img.setOnClickListener {
            onBackPressed()
        }

        refresh_layout.setOnRefreshListener {

            refresh_layout.isRefreshing = false

            getOldDataByIssueNo(OldItem.issueNo)
        }

        box_no_edit.doAfterTextChanged{
                val b: String = box_no_edit.getText().toString()
                if (b.length > 10) {
                    getOldBox(b)
                } else {
                    // Toast.makeText(this,"Enter the number of 12 digits", Toast.LENGTH_LONG).show()
                }

                box_no_edit.getText().clear()
                box_no_edit.setFocusable(true)
                box_no_edit.requestFocus()
            }

        refNo_txt.setText(OldItem.issueNo)
        getOldDataByIssueNo(OldItem.issueNo)

        completeBox_txt.setOnClickListener {
            onBackPressed()
        }
    }

    private fun getOldDataByIssueNo(issueNo:String){
        try {
            con = connectionClass(SERVER_NAME_DA ,SERVER_PASSWORD_DA, DATABASE_NAME_DA, SERVER_IP_DA)
            if (con == null) {
                Toast.makeText(this, "Server Link Fail Try Again ....", Toast.LENGTH_LONG).show()
            } else {
                val sql = "SELECT IssueRefNo, SAPPoductCode, COUNT(BatchNo) AS Qty FROM VW_DESPATCH_SUMMARY where IssueRefNo ='"+issueNo+"' GROUP BY IssueRefNo, SAPPoductCode"
                val smt = con!!.createStatement()
                val data = smt.executeQuery(sql)
                var row  = data.row
                newItemList.clear()
                while (data.next()) {
                    row=row+1
                    var b = data.row
                    val IssueRefNo = data.getString("IssueRefNo")
                    val SAPPoductCode = data.getString("SAPPoductCode")
                    val Qty = data.getString("Qty")
                    val NewItemModel = NewItemModel(SAPPoductCode,"",Qty)
                    newItemList.add(NewItemModel)
                    showNewItemList(newItemList)
                }
            }

        }catch (e: Exception){
            Log.e("TAg",e.message!!)
        }
    }

    private fun getOldBox(scanBox:String){
        try {
            con = connectionClass(SERVER_NAME_DA ,SERVER_PASSWORD_DA, DATABASE_NAME_DA, SERVER_IP_DA)
            if (con == null) {
                Toast.makeText(this, "Server Link Fail Try Again ....", Toast.LENGTH_LONG).show()
            } else {
                val sql = "select * from tabIssueedBatch where BatchNo ='"+scanBox+"'"
                val smt = con!!.createStatement()
                val data = smt.executeQuery(sql)
                if (data.next()) {
                    val IssueRefNo = data.getString("IssueRefNo")
                    if (IssueRefNo!=null){
                        Toast.makeText(this,"Already Scanned.....",Toast.LENGTH_LONG).show()
                        alreadyScan(IssueRefNo)
                        playSound(R.raw.error)
                    }else{
                        Toast.makeText(this,"Scanned in Slip No: $IssueRefNo",Toast.LENGTH_LONG).show()
                        playSound(R.raw.error)
                    }
                //getItemAndQty(scanBox)
                }else{
                    getItemAndQty(scanBox)
                }
            }

        }catch (e: Exception){
            Log.e("TAg",e.message!!)
        }
    }


    private fun getItemAndQty(item:String){
        try {
            con = connectionClass(SERVER_NAME_DA ,SERVER_PASSWORD_DA, DATABASE_NAME_DA, SERVER_IP_DA)
            if (con == null) {
                Toast.makeText(this, "Server Link Fail Try Again ....", Toast.LENGTH_LONG).show()
            } else {
                val sql = "select * from VW_BATCH_STOCK_LIST where BatchNo ='"+item+"'"
                val smt = con!!.createStatement()
                val data = smt.executeQuery(sql)
                if (data.next()) {
                   val ITEMCODE = data.getString("SAPPoductCode")
                    val ItemName = data.getString("MatDesc")
                    val qty = data.getString("PackQty")
                    val newItemModel = NewItemModel(ITEMCODE,ItemName,qty)
                    newItemList.add(newItemModel)
                    showNewItemList(newItemList)
                        if (OldItem.issueNo.equals("")){
                            val sql1 = "select isnull(max(IssueNo),900000)+1 as issue from tblDespetch"
                            val smt = con!!.createStatement()
                            val data = smt.executeQuery(sql1)
                            if (data.next()){
                                OldItem.issueNo = data.getString("issue")
                                refNo_txt.setText(OldItem.issueNo)
                                val sql2 = "INSERT INTO tblDespetch (IssueNo,DispatchDate,Closed) VALUES('"+data.getString("issue")+"',getdate(),0)"
                                val sqr4 = "insert into tabIssueedBatch(BatchNo,IssueDate,Employee,IssueRefNo,SeqNo) values ('"+item+"',getdate(),'"+sharedpreferences!!.getString("USERNAME","")+"','"+data.getString("issue")+"',0)"
                                smt.execute(sql2)
                                smt.execute(sqr4)
                               Toast.makeText(this,"Data Is Stored....",Toast.LENGTH_LONG).show()
                            }
                        }else{
                            val sqr4 = "insert into tabIssueedBatch(BatchNo,IssueDate,Employee,IssueRefNo,SeqNo) values ('"+item+"',getdate(),'"+sharedpreferences!!.getString("USERNAME","")+"','"+OldItem.issueNo+"',0)"
                            smt.execute(sqr4)
                            Toast.makeText(this,"Data Is Stored....",Toast.LENGTH_LONG).show()
                        }

                }else{
                Toast.makeText(this,"Not Found In Stock.....",Toast.LENGTH_LONG).show()
                    playSound(R.raw.error)
                }
            }

        }catch (e: Exception){
            Log.e("TAg",e.message!!)
        }
    }

    private fun showNewItemList(newItemList: List<NewItemModel>){
        val newItemAdapter = NewItemAdapter(newItemList,this)
        val layoutManage = LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false)
        newitemCode_recyclerview.layoutManager = layoutManage
        newitemCode_recyclerview.adapter = newItemAdapter
    }


    fun connectionClass(
        user: String,
        password: String,
        database: String,
        server: String
    ): Connection? {
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        var connection: Connection? = null
        var connectionURL: String? = null
        try {
            Class.forName("net.sourceforge.jtds.jdbc.Driver")
            connectionURL = "jdbc:jtds:sqlserver://$server/$database;user=$user;password=$password;"
            connection = DriverManager.getConnection(connectionURL)
        } catch (e: Exception) {
            Log.e("SQL Connection Error : ", e.message!!)
        }
        return connection
    }

    override fun onItemClick(newItemModel: NewItemModel) {
    }


    private fun playSound(resId: Int) {
        val mp = MediaPlayer.create(this, resId)
        mp.setOnCompletionListener { mediaPlayer ->
            mediaPlayer.reset()
            mediaPlayer.release()
        }
        mp.start()
    }

    private fun alreadyScan(issueNumber: String){
        val dialog = Dialog(this,R.style.ThemeOverlay_MaterialComponents_MaterialAlertDialog_Background)
        dialog.setContentView(R.layout.already_scan)
        val already = dialog.findViewById<TextView>(R.id.alreadyScan_txt)
        already.text = "Already Scanned: $issueNumber"

        dialog.show()
    }
}