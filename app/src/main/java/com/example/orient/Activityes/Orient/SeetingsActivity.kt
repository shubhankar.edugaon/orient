package com.example.orient.Activityes.Orient

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.os.StrictMode
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.widget.Toast
import com.example.orient.R
import com.example.orient.SessionManager
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_login.view.*
import kotlinx.android.synthetic.main.activity_seetings.*
import kotlinx.android.synthetic.main.activity_seetings.view.*
import kotlinx.android.synthetic.main.settings_layout.view.*
import java.sql.Connection
import java.sql.DriverManager
import java.sql.Statement

class SeetingsActivity : LoaderActivity() {
    var SERVER_IP_DA = ""
    private val context: Context = this
    var stmt: Statement? = null
    var con: Connection? = null
    val lineNumber_valid = ""
    val  printerIp_valid = ""
    val  printPort_valid = ""
    private var sessionManager: SessionManager? = null
    val SHARED_PREFS = "shared_prefs"
    val SERVER_IP = "SERVER_IP"
    val SERVER_PASSWORD = "SERVER_PASSWORD"
    val SERVER_NAME = "SERVERNAME"
    val DATABASE_NAME = "DATABASE"
    val PRINTER_PORT = "PRINTER_PORT"
    val PRINTER_IP = "PRINTER_IP"
    val USERNAME = "USERNNAME"
    val LINENUMBER = "LINENUMBER"
    val PRINTIP = ""
    val PRINTPORT = ""
    val LINE = ""
    var sharedpreferences: SharedPreferences? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_seetings)
        sessionManager = SessionManager(this@SeetingsActivity)
        sharedpreferences = getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE)

//        setting_arrowBack.setOnClickListener {
//            onBackPressed()
//        }

        sharedpreferences = getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE)
       val SERVER_IP_DA = sharedpreferences?.getString(SERVER_IP,"").toString()
       val SERVER_PASSWORD = sharedpreferences?.getString(SERVER_PASSWORD,"").toString()
        val SERVER_NAME = sharedpreferences?.getString(SERVER_NAME,"").toString()
       val DATABASE_NAME = sharedpreferences?.getString(DATABASE_NAME,"").toString()

        val PRINT_IP = sharedpreferences?.getString(PRINTER_IP,"").toString()
        val PRINT_PORT = sharedpreferences?.getString(PRINTER_PORT,"").toString()
        val LINE_NUMBER = sharedpreferences?.getString(LINENUMBER,"").toString()


        serverIp_edit.setText(SERVER_IP_DA)
        serverUserName_edit.setText(SERVER_NAME)
        serverPassword_edit.setText(SERVER_PASSWORD)
        databaseName_edit.setText(DATABASE_NAME)

        workLineNumber_edit.setText(LINE_NUMBER)
        printerIp_edit.setText(PRINT_IP)
        printerPort_edit.setText(PRINT_PORT)

        databaseName_edit.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_ENTER && event.action == KeyEvent.ACTION_UP) {

                return@OnKeyListener true
                databaseName_edit.clearFocus()
                databaseName_edit.text.clear()
            }
            false
        })

        printSaveBtn.setOnClickListener {
            storePrinter()

        }
        plantSaveBtn.setOnClickListener {
            storePlantDetails()
        }
        save_button1.setOnClickListener {
            connectToDb()
        }
    }

    @SuppressLint("SuspiciousIndentation")
    private fun connectToDb() {
        val serverIp_valid = serverIp_edit.text.toString()
        val serverUsername_valid = serverUserName_edit.text.toString()
        val serverPass_valid = serverPassword_edit.text.toString().trim()
        val databasename_valid = databaseName_edit.text.toString().trim()
        val lineNumber_valid = workLineNumber_edit.text.toString().trim()
        val  printerIp_valid = printerIp_edit.text.toString().trim()
        val  printPort_valid = printerPort_edit.text.toString().trim()
        if (serverIp_valid.isEmpty()){
            serverIp_edit.error = "Enter Server Ip"
                serverIp_edit.requestFocus()
        }else if (serverUsername_valid.isEmpty()){
            serverUserName_edit.error = "Enter User Name"
            serverUserName_edit.requestFocus()
        }else if (serverPass_valid.isEmpty()){
            serverPassword_edit.error = "Enter server Password"
            serverPassword_edit.requestFocus()
        }else if (databasename_valid.isEmpty()){
            databaseName_edit.error = "Enter Database Name"
            databaseName_edit.requestFocus()
        }else{
            sessionManager?.SettingsDetails(serverIp_valid,serverPass_valid,serverUsername_valid,databasename_valid,lineNumber_valid,printerIp_valid,printPort_valid,"")
        }
        Toast.makeText(this.applicationContext,"Data Stored", Toast.LENGTH_LONG).show()
        ConntectToServer(serverUsername_valid,serverPass_valid,databasename_valid,serverIp_valid)
        LoaderActivity()
    }

    private fun ConntectToServer(server: String,password: String,database: String,serverIp_valid:String){
        try {
            con = connectionClass(server ,password, database, serverIp_valid)
            if (con == null) {
                dismissLoaderActivity()
                Toast.makeText(this, "Server Link Fail Try Again ....", Toast.LENGTH_LONG).show()
            } else {
                dismissLoaderActivity()
                Toast.makeText(this, "Server Connedcted"+serverIp_valid, Toast.LENGTH_LONG).show()
                next_button1.visibility = View.VISIBLE
                save_button1.visibility = View.GONE
                sessionManager?.SettingsDetails(serverIp_valid,password,server,database,lineNumber_valid,printerIp_valid,printPort_valid,"Connected")
                next_button1.setOnClickListener{
                    serverDetails_layout.visibility = View.GONE
                    //printDetails_layout.visibility = View.VISIBLE
                    plantDetails_layout.visibility = View.VISIBLE
                }
            }
        }catch (e: Exception){
            dismissLoaderActivity()
            Log.e("TAg",e.message!!)
        }
    }

    @SuppressLint("NewApi")
    fun connectionClass(user: String, password: String, database: String, server: String): Connection? {
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        var connection: Connection? = null
        var connectionURL: String? = null
        try {
            Class.forName("net.sourceforge.jtds.jdbc.Driver")
            connectionURL = "jdbc:jtds:sqlserver://$server/$database;user=$user;password=$password;"
            connection = DriverManager.getConnection(connectionURL)
        } catch (e: Exception) {
            Log.e("SQL Connection Error : ", e.message!!)
        }
        return connection
    }

    private fun storePrinter(){
        val printIp = printerIp_edit.text.toString().trim()
            val lineNumber = workLineNumber_edit.text.toString().trim()
        val printerPort = printerPort_edit.text.toString().trim()
        if (printIp.isEmpty()){
            printerIp_edit.error = "Please Enter Printer Ip"
        }else if (lineNumber.isEmpty()){
            workLineNumber_edit.error = "Please Enter Line Number"
        }else if (printerPort.isEmpty()){
            printerPort_edit.error = "Please Enter Print Port Number"
        }else{
            sessionManager?.PrinterDetails(printIp,printerPort,lineNumber,"Saved")
            Toast.makeText(this,"Saved",Toast.LENGTH_LONG).show()
            startActivity(Intent(this, LoginActivity::class.java))
        }
    }

    fun storePlantDetails(){
        sessionManager?.PlantDetails(plantNumber_edit.text.toString(),lineNumber_edit.text.toString(),settingPass_edit.text.toString())
        Toast.makeText(this,"Saved",Toast.LENGTH_LONG).show()
        startActivity(Intent(this, LoginActivity::class.java))
    }
    }
