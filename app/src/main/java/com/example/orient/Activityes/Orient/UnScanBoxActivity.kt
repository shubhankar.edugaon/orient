package com.example.orient.Activityes.Orient

import android.app.Dialog
import android.content.Context
import android.content.SharedPreferences
import android.media.MediaPlayer
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.StrictMode
import android.service.autofill.UserData
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.core.widget.doAfterTextChanged
import com.example.orient.OrientDataModel.OldItem
import com.example.orient.R
import com.example.orient.SessionManager
import kotlinx.android.synthetic.main.activity_box_paking.*
import kotlinx.android.synthetic.main.activity_new_scan.*
import kotlinx.android.synthetic.main.activity_un_scan_box.*
import java.sql.Connection
import java.sql.DriverManager

class UnScanBoxActivity : AppCompatActivity() {
    val USERNAME = "USERNNAME"
    val SERVER_IP = "SERVER_IP"
    val SERVER_PASSWORD = "SERVER_PASSWORD"
    val SERVER_NAME = "SERVERNAME"
    val DATABASE_NAME = "DATABASE"
    val LINENUMBER = "LINENUMBER"
    val PRINTER_IP = "PRINTER_IP"
    val PRINTER_PORT = "PRINTER_PORT"
    val PRN  = "PRN"
    val SHARED_PREFS = "shared_prefs"
    var con: Connection? = null
    var sharedpreferences: SharedPreferences? = null
    private var sessionManager: SessionManager? = null
    var SERVER_IP_DA = ""
    var SERVER_PASSWORD_DA = ""
    var SERVER_NAME_DA = ""
    var DATABASE_NAME_DA = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_un_scan_box)

        sessionManager = SessionManager(this@UnScanBoxActivity)
        sharedpreferences = getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE)
        unscanBox_userName.text = sharedpreferences?.getString(USERNAME,"").toString()
        SERVER_IP_DA = sharedpreferences?.getString(SERVER_IP,"").toString()
        SERVER_PASSWORD_DA = sharedpreferences?.getString(SERVER_PASSWORD,"").toString()
        SERVER_NAME_DA = sharedpreferences?.getString(SERVER_NAME,"").toString()
        DATABASE_NAME_DA = sharedpreferences?.getString(DATABASE_NAME,"").toString()
        unscanBoxUserName.text = com.example.orient.OrientDataModel.UserData.userName


        unscanBoxArrowBack_img.setOnClickListener {
            onBackPressed()
        }


        unscanBox_edit.doAfterTextChanged{
                val b: String = unscanBox_edit.text.toString()
                if (b.length > 10){
                    unloadBox(b)
                }else{
                }
                unscanBox_edit.getText().clear()
                unscanBox_edit.setFocusable(true)
                unscanBox_edit.requestFocus()

            }


    }


    private fun unloadBox(unScanBox:String){
        try {
            con = connectionClass(SERVER_NAME_DA ,SERVER_PASSWORD_DA, DATABASE_NAME_DA, SERVER_IP_DA)
            if (con == null) {
                Toast.makeText(this, "Server Link Fail Try Again ....", Toast.LENGTH_LONG).show()
            } else {
                val sql = "select * from tblDespetch where IssueNo ='"+OldItem.issueNo+"'"
                val smt = con!!.createStatement()
                val data = smt.executeQuery(sql)
                if (data.next()) {
                        val sql1 = "SELECT tabIssueedBatch.BatchNo, tblDespetch.IssueNo, tblDespetch.Closed FROM   tabIssueedBatch INNER JOIN  tblDespetch ON tabIssueedBatch.IssueRefNo = tblDespetch.IssueNo where tabIssueedBatch.BatchNo='"+unScanBox+"' and tblDespetch.IssueNo ='"+OldItem.issueNo+"'"
                        val data1 = smt.executeQuery(sql1)
                        if (data1.next()){
                                    val deleteQuery = "delete tabIssueedBatch where BatchNo='"+data1.getString("BatchNo")+"'"
                                         smt.execute(deleteQuery)
                            Toast.makeText(this,"Delete From Load List.....",Toast.LENGTH_LONG).show()
                            deleteFromLoad(unScanBox)
                        }else{
                            Toast.makeText(this,"Not Found In Load List.....",Toast.LENGTH_LONG).show()
                            playSound(R.raw.error)
                        }

                }else{
                    Toast.makeText(this, "Unload Not Allow Its Closed.....", Toast.LENGTH_LONG).show()
                    playSound(R.raw.error)
                }
            }

        }catch (e: Exception){
            Log.e("TAg",e.message!!)
            Toast.makeText(this, "Unload Not Allow Its Closed.....", Toast.LENGTH_LONG).show()
            playSound(R.raw.error)
        }
    }


    fun connectionClass(
        user: String,
        password: String,
        database: String,
        server: String
    ): Connection? {
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        var connection: Connection? = null
        var connectionURL: String? = null
        try {
            Class.forName("net.sourceforge.jtds.jdbc.Driver")
            connectionURL = "jdbc:jtds:sqlserver://$server/$database;user=$user;password=$password;"
            connection = DriverManager.getConnection(connectionURL)
        } catch (e: Exception) {
            Log.e("SQL Connection Error : ", e.message!!)
        }
        return connection
    }

    private fun playSound(resId: Int) {
        val mp = MediaPlayer.create(this, resId)
        mp.setOnCompletionListener { mediaPlayer ->
            mediaPlayer.reset()
            mediaPlayer.release()
        }
        mp.start()
    }

    private fun deleteFromLoad(issueNumber: String){
        val dialog = Dialog(this,R.style.ThemeOverlay_MaterialComponents_MaterialAlertDialog_Background)
        dialog.setContentView(R.layout.already_scan)
        val already = dialog.findViewById<TextView>(R.id.alreadyScan_txt)
        already.text = "Delete From Load List: $issueNumber"
        dialog.show()
    }
}