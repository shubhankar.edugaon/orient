package com.example.orient.Activityes.Orient

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.SharedPreferences
import android.media.MediaPlayer
import android.os.AsyncTask
import android.os.Bundle
import android.os.StrictMode
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.Button
import android.widget.TextView.OnEditorActionListener
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.orient.OrientDataModel.printeModel
import com.example.orient.R
import com.example.orient.SessionManager
import kotlinx.android.synthetic.main.activity_box_paking.*
import kotlinx.android.synthetic.main.activity_warrant_label.*
import java.io.DataOutputStream
import java.io.IOException
import java.io.PrintWriter
import java.net.Socket
import java.sql.Connection
import java.sql.DriverManager


class WarrantLabelActivity : AppCompatActivity() {
    val USERNAME = "USERNNAME"
    val SERVER_IP = "SERVER_IP"
    val SERVER_PASSWORD = "SERVER_PASSWORD"
    val SERVER_NAME = "SERVERNAME"
    val DATABASE_NAME = "DATABASE"
    val LINENUMBER = "LINENUMBER"
    val PRINTER_IP = "PRINTER_IP"
    val PRINTER_PORT = "PRINTER_PORT"
    val WARRANTYPRN = "WARRANTYPRN"
    val SHARED_PREFS = "shared_prefs"
    var con: Connection? = null
    var sharedpreferences: SharedPreferences? = null
    private var sessionManager: SessionManager? = null
    var SERVER_IP_DA = ""
    var SERVER_PASSWORD_DA = ""
    var SERVER_NAME_DA = ""
    var DATABASE_NAME_DA = ""
    var warrantyPackSize=0
    private var warrantyRequired = ArrayList<String>()
    private var warrantyList = ArrayList<String>()
    var count = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_warrant_label)

        warrantyLabeArrowBack_img.setOnClickListener {
            onBackPressed()
        }

        warrantyList = ArrayList<String>()
        for (i in 1..4){
            warrantyRequired.add(i.toString())
        }
        warranty_number_txt.text = "/"+warrantyRequired!!.size.toString()
        sessionManager = SessionManager(this@WarrantLabelActivity)
        sharedpreferences = getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE)
        warrantyIpAndPort_txt.text = sharedpreferences?.getString(PRINTER_IP,"").toString()+":"+sharedpreferences?.getString(PRINTER_PORT,"").toString()
        warrantyLabel_userName.text = sharedpreferences?.getString(USERNAME,"").toString()
        SERVER_IP_DA = sharedpreferences?.getString(SERVER_IP,"").toString()
        SERVER_PASSWORD_DA = sharedpreferences?.getString(SERVER_PASSWORD,"").toString()
        SERVER_NAME_DA = sharedpreferences?.getString(SERVER_NAME,"").toString()
        DATABASE_NAME_DA = sharedpreferences?.getString(DATABASE_NAME,"").toString()

        printeModel.printerIp = sharedpreferences?.getString(PRINTER_IP,"").toString()
        printeModel.printerPort = sharedpreferences?.getString(PRINTER_PORT,"").toString()

        warrantyLabel_editText.setOnKeyListener(object : View.OnKeyListener {
            override fun onKey(view: View?, keyCode: Int, keyevent: KeyEvent): Boolean {
                return if (keyCode == KeyEvent.KEYCODE_ENTER ) {
                    val b: String = warrantyLabel_editText.getText().toString()
                    if (b.equals("..")){
                            count = 0
                        warrantyTotalPacking_txt.text = "Total Scan: "+count.toString()
                        warrantyLastScanNo_txt.text = "XXXXXXXXXXXXXXXX"
                        warrantyPartNo.text = "XXXXXXXXXXXXXXXX"
                        WarrantyModelNumber_txt.text = "XXXXXXXXXXXXXXXX"
                        warrantyTxt.text = "XXXXXXXXXXXXXXXX"

                    }else if (b.length > 10)
                    {
                        val listData = b
                        if (warrantyList!!.contains(listData)) {
                            Toast.makeText(this@WarrantLabelActivity,"Already added$listData", Toast.LENGTH_LONG).show()
                            playSound(R.raw.error)
                        }
                        else
                        {
                            validateBarcode(listData)
                        }
                    } else {
                       // Toast.makeText(this@WarrantLabelActivity,"Enter the number of 12 digits", Toast.LENGTH_LONG).show()
                    }

                    warrantyLabel_editText.getText().clear()
                    warrantyLabel_editText.setFocusable(true)
                    warrantyLabel_editText.requestFocus()
                    true
                }
                else {
                    Toast.makeText(this@WarrantLabelActivity,"False", Toast.LENGTH_LONG).show()
                    false
                }
            }
        })


        GetPrnData()
    }

    private fun GetPrnData(){
        try {
            con = connectionClass(SERVER_NAME_DA ,SERVER_PASSWORD_DA, DATABASE_NAME_DA, SERVER_IP_DA)
            if (con == null) {
                Toast.makeText(this, "Server Link Fail Try Again ....", Toast.LENGTH_LONG).show()
            } else {
                val sql = "SELECT  BoxPrn FROM tabBoxPackingPrn where boxprnid='WARRANTY'"
                val smt = con!!.createStatement()
                val data = smt.executeQuery(sql)
                if (data.next()) {
                    val warrantyPrn = data.getString("BoxPrn")
                    sessionManager?.SaveWarrantyPrn(warrantyPrn)
                    printeModel.warrantyPrn = sharedpreferences?.getString("WARRANTYPRN","").toString()
                } else {
                    Toast.makeText(this, "Prn Getting Fail....", Toast.LENGTH_LONG).show()
                }
            }
        }catch (e: Exception){
            Log.e("TAg",e.message!!)
        }
    }

    @SuppressLint("SuspiciousIndentation")
    private fun validateBarcode(barcode:String){
        try {
            con = connectionClass(SERVER_NAME_DA ,SERVER_PASSWORD_DA, DATABASE_NAME_DA, SERVER_IP_DA)
            if (con == null) {
                Toast.makeText(this, "Server Link Fail Try Again ....", Toast.LENGTH_LONG).show()
            } else {
                val sql = "Select * from VW_SERIALNO_INFO where EXISENO='"+barcode+"'"
                val smt = con!!.createStatement()
                val data = smt.executeQuery(sql)
                if(data.next()) {
                    if(!data.getBoolean("Blocked"))
                    {

                        if(!data.getBoolean("Scanned"))
                        {
                            warrantyLastScanNo_txt.text = data.getString("EXISENO")
                            warrantyPartNo.text = data.getString("PoductCode")
                            WarrantyModelNumber_txt.text = data.getString("Model")
                            warrantyTxt.text= data.getString("Color")+"/"+data.getString("Sweeps")
                            addData(data.getString("EXISENO"))
                        }
                        else
                        {
                            Toast.makeText(this, "Already Packed On Box :"+data.getString("BoxNo"), Toast.LENGTH_LONG).show()
                            addAlertDialog(data.getString("EXISENO"))
                            playSound(R.raw.error)

                        }
                    }
                    else
                    {
                        Toast.makeText(this, "Blocked Serial No....", Toast.LENGTH_LONG).show()
                        playSound(R.raw.error)
                    }

                }
                else
                {
                    Toast.makeText(this, "Invalid Serial No....", Toast.LENGTH_LONG).show()



                }
            }
        }catch (e: Exception){
            Log.e("TAg",e.message!!)
        }
    }

    private fun addData(Exiseno: String) {
        try {
            con = connectionClass(SERVER_NAME_DA ,SERVER_PASSWORD_DA, DATABASE_NAME_DA, SERVER_IP_DA)
            if (con == null) {
                Toast.makeText(this, "Server Link Fail Try Again ....", Toast.LENGTH_LONG).show()
            } else {
                val sql = "insert into tabWarrantyPrintLog(LogBy,ExiseNo) values('"+sharedpreferences?.getString(USERNAME,"").toString()+"','"+Exiseno+"')"
                val smt = con!!.createStatement()
                val data = smt.executeQuery(sql)
                if(data.next()) {
                    val boxNumber = data.getString("ExiseNo")
                    PrintPacketLabel(boxNumber)
                }
                else
                {
                    Toast.makeText(this, "Invalid Serial No....", Toast.LENGTH_LONG).show()
                }
            }
        }catch (e: Exception){
            Log.e("TAg",e.message!!)
        }
    }

    private fun PrintPacketLabel(boxNo :String){
        try {
            con = connectionClass(SERVER_NAME_DA ,SERVER_PASSWORD_DA, DATABASE_NAME_DA, SERVER_IP_DA)
            if (con == null) {
                Toast.makeText(this, "Server Link Fail Try Again ....", Toast.LENGTH_LONG).show()
            } else {
                var prn = printeModel.warrantyPrn
                val sql = "select * from VW_PRINT_UNIT_SERIAL_NO where ExiseNo='"+boxNo+"'"
                val smt = con!!.createStatement()
                val data = smt.executeQuery(sql)
                var row : Int = 0
                val column = data.getMetaData().getColumnCount()
                while (data.next()) {
                    for(c in 1..column){
                        val b = data.getString(c)
                        prn=prn.replace("<FE/>"+data.getMetaData().getColumnName(c)+"</FE>",data.getString(c).toString(),true)
                    }
                }
                val messageSender = MessageSender()
                messageSender.execute(prn)
                count++
                warrantyLastScanNo_txt.text = boxNo
                warrantyTotalPacking_txt.setText("Total Scan: " + count)
            }
        }catch (e: Exception){
            Log.e("TAg",e.message!!)
        }
    }

    class MessageSender : AsyncTask<String?, Void?, Void?>() {
        var s: Socket? = null
        var pw: PrintWriter? = null
        var dos: DataOutputStream? = null
        protected override fun doInBackground(vararg voids: String?): Void? {
            val message = voids[0]
            try {
                s = Socket(printeModel.printerIp,printeModel.printerPort.toInt())
                pw = PrintWriter(s!!.getOutputStream())
                pw!!.write(message)
                pw!!.flush()
                pw!!.close()
                s!!.close()
            } catch (ex: IOException) {
                ex.printStackTrace()
            }
            return null
        }
    }

    private fun  addAlertDialog(boxNo: String){
        val dialog = Dialog(this,R.style.ThemeOverlay_MaterialComponents_MaterialAlertDialog_Background)
        dialog.setContentView(R.layout.custom_dialog1)
        val yesBtn = dialog.findViewById<Button>(R.id.yes1_btn)
        val noBtn = dialog.findViewById<Button>(R.id.no1_btn)
        yesBtn.setOnClickListener {
            PrintPacketLabel(boxNo)
            dialog.dismiss()
        }

        noBtn.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }

    private fun playSound(resId: Int) {
        val mp = MediaPlayer.create(this, resId)
        mp.setOnCompletionListener { mediaPlayer ->
            mediaPlayer.reset()
            mediaPlayer.release()
        }
        mp.start()
    }

    fun connectionClass(user: String, password: String, database: String, server: String): Connection? {
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        var connection: Connection? = null
        var connectionURL: String? = null
        try {
            Class.forName("net.sourceforge.jtds.jdbc.Driver")
            connectionURL = "jdbc:jtds:sqlserver://$server/$database;user=$user;password=$password;"
            connection = DriverManager.getConnection(connectionURL)
        } catch (e: Exception) {
            Log.e("SQL Connection Error : ", e.message!!)
        }
        return connection
    }

    fun onKey(v: View?, keyCode: Int, event: KeyEvent): Boolean {
        if (event.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
            Log.i("a", "b")
            return true
        }
        return false
    }
}