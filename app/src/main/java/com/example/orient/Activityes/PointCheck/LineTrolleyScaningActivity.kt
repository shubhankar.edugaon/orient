package com.example.orient.Activityes.PointCheck

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.media.MediaPlayer
import android.os.Bundle
import android.os.StrictMode
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.Window
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.orient.CheckPointModel.DataModel
import com.example.orient.OrientDataModel.printeModel
import com.example.orient.R
import com.example.orient.SessionManager
import kotlinx.android.synthetic.main.activity_line_trolley_scaning.line_value_txt
import kotlinx.android.synthetic.main.activity_line_trolley_scaning.next_btn
import kotlinx.android.synthetic.main.activity_line_trolley_scaning.shift_value_txt
import kotlinx.android.synthetic.main.activity_line_trolley_scaning.trolley_barcode_edit
import kotlinx.android.synthetic.main.activity_line_trolley_scaning.user_value_txt
import java.sql.Connection
import java.sql.DriverManager
import java.sql.ResultSet

class LineTrolleyScaningActivity : AppCompatActivity() {

    val USERNAME = "USERNNAME"
    val SERVER_IP = "SERVER_IP"
    val SERVER_PASSWORD = "SERVER_PASSWORD"
    val SERVER_NAME = "SERVERNAME"
    val DATABASE_NAME = "DATABASE"
    val TS_LINENUMBER = "TS_LINENUMBER"
    val SHARED_PREFS = "shared_prefs"
    var con: Connection? = null
    var sharedpreferences: SharedPreferences? = null
    private var sessionManager: SessionManager? = null
    var arrayAdapter: ArrayAdapter<String>? = null
    var SERVER_IP_DA = ""
    var SERVER_PASSWORD_DA = ""
    private var checked = false
    private var ValidTrollyNo = false
    var SERVER_NAME_DA = ""
    var DATABASE_NAME_DA = ""
    var packedQty = ""
    var InputBoxValue = ""
    var count: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_line_trolley_scaning)

        sessionManager = SessionManager(this@LineTrolleyScaningActivity)
        sharedpreferences = getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE)
        SERVER_IP_DA = sharedpreferences?.getString(SERVER_IP,"").toString()
        SERVER_PASSWORD_DA = sharedpreferences?.getString(SERVER_PASSWORD,"").toString()
        SERVER_NAME_DA = sharedpreferences?.getString(SERVER_NAME,"").toString()
        DATABASE_NAME_DA = sharedpreferences?.getString(DATABASE_NAME,"").toString()

        line_value_txt.text = sharedpreferences?.getString(TS_LINENUMBER,"").toString()
        shift_value_txt.text = DataModel.shift.toString()
        val Uname = sharedpreferences?.getString(USERNAME,"").toString()
        if (Uname !=null){
            user_value_txt.text = Uname
        }else{
        user_value_txt.text = "XXXXXXXXXXXXX"
        }


        next_btn.setOnClickListener {
            startActivity(Intent(this,ScanDetailsActivity::class.java))
        }

            trolley_barcode_edit.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
                if (keyCode == KeyEvent.KEYCODE_ENTER && event.action == KeyEvent.ACTION_UP) {
                    val data = trolley_barcode_edit.text.toString()
                    if (data.length > 0){
                        checkTrolley(data)
                    }
                    //Perform Code
                    trolley_barcode_edit.text.clear()
                    trolley_barcode_edit.clearFocus()
                    return@OnKeyListener true
                }
                false
            })
    }

    private fun checkTrolley(masterCode:String){
        try {
            con = getConnection(SERVER_NAME_DA ,SERVER_PASSWORD_DA, DATABASE_NAME_DA, SERVER_IP_DA)
            if (con == null) {
                Toast.makeText(this, "Server Link Fail Try Again ....", Toast.LENGTH_LONG).show()
            } else {
                val sql = "Select * from VW_GENERATED_TROLLY  where TrollyTagID='"+masterCode+"'"
                val smt = con!!.createStatement()
                val data = smt.executeQuery(sql)
                var row = 0
                if (data.next()) {
                   DataModel.PPlanNo = data.getString("ProdPlanNo").toString()
                    DataModel.PSeq = data.getString("ProdPlanSeqNo").toString()
                    DataModel.targetQty = data.getString("BabyBoxesQty").toString()
                    DataModel.lblPartcode = data.getString("ItemCode").toString().toUpperCase()
                    DataModel.lblPartName = data.getString("ItemDescription").toString().toUpperCase()
                    DataModel.lblJobNo = masterCode
                    if (data.getString("itemcode").toString().equals(DataModel.partCode)){

                    }else if (data.getString("ProdLineNo").toString().equals(sharedpreferences?.getString(TS_LINENUMBER,"").toString())){
                        val qu = "Select * from tabProdPlan  where ProdPlanNo='"+data.getString("ProdPlanNo").toString()+"'"
                        val data1 =smt.executeQuery(qu)
                        if (data1.next()){
                                if (data1.getString("Status").toString().toUpperCase().equals("PENDING")){
                                    count = 0
                                    val sql1 = "select MasterBoxNo From tabBabyBoxPrintLog where MasterBoxNo='"+masterCode+"'"
                                    val smt = con!!.createStatement()
                                    val data = smt.executeQuery(sql1)
                                    while (data.next()){
                                            count++
                                        data.getString("MasterBoxNo").toString()
                                    }
                                    Toast.makeText(this@LineTrolleyScaningActivity,""+count.toString(),Toast.LENGTH_LONG).show()
                                    DataModel.packedQty = count.toString()
                                    if (count.toString().length <= 0){
                                        showDialog()
                                        if (InputBoxValue.length > 0){
                                            var TrollyAssetNo = InputBoxValue
                                            if (ValidTrollyNo(TrollyAssetNo) !=null){
                                                Toast.makeText(this,"Invalid Trolley Asset No.. Try Again",Toast.LENGTH_LONG).show()
                                            }else{
                                                SetTrollyAssetNo(masterCode,InputBoxValue)
                                            }
                                        }else{
                                            Toast.makeText(this@LineTrolleyScaningActivity,"Trolley Asset No Set Fail..",Toast.LENGTH_LONG).show()
                                            playSound(R.raw.error)
                                        }
                                    }else{ }
                                    startActivity(Intent(this,ScanDetailsActivity::class.java))
//
                                }else{
                                    Toast.makeText(this,"Production Order :"+data1.getString("Status").toString().toUpperCase(),Toast.LENGTH_LONG).show()
                                }
                        }else{
                            Toast.makeText(this,"Invalid Production Order..", Toast.LENGTH_LONG).show()
                        }
                    }else{
                        Toast.makeText(this,"Not For This Line....", Toast.LENGTH_LONG).show()
                    }

                } else {
                    Toast.makeText(this, "Invalid Trolley Barcode Scanned...", Toast.LENGTH_LONG).show()
                    playSound(R.raw.error)
                }
            }
        }catch (e: Exception){
            playSound(R.raw.error)
            Toast.makeText(this,"Server Link Fail Try Again ....", Toast.LENGTH_LONG).show()
        }
    }

    private fun ValidTrollyNo(Partcode:String){
            try {
                con = getConnection(SERVER_NAME_DA ,SERVER_PASSWORD_DA, DATABASE_NAME_DA, SERVER_IP_DA)
                if (con == null) {
                    Toast.makeText(this, "Server Link Fail Try Again ....", Toast.LENGTH_LONG).show()
                } else {
                    val sql = "Select * from tabTrollyMst where TrollyID='"+Partcode+"'"
                    val smt = con!!.createStatement()
                    val data = smt.executeQuery(sql)
                    if (data.next()) {
                            ValidTrollyNo = true
                    } else {
                        Toast.makeText(this, "Invalid Trolley Asset No.. Try Again", Toast.LENGTH_LONG).show()
                    }
                }
            }catch (e: Exception){
                Log.e("TAg",e.message!!)
            }
        }

    private fun SetTrollyAssetNo(packetNo:String,TrollyAssetNo:String){
        try {
            con = getConnection(SERVER_NAME_DA ,SERVER_PASSWORD_DA, DATABASE_NAME_DA, SERVER_IP_DA)
            if (con == null) {
                Toast.makeText(this, "Server Link Fail Try Again ....", Toast.LENGTH_LONG).show()
            } else {
                val sql = "update tabBoxPrintLog set TrollyID='"+TrollyAssetNo+"' where BoxNo='"+packetNo+"'"
                val smt = con!!.createStatement()
                val data = smt.executeQuery(sql)
                if (data.next()) {
                    ValidTrollyNo = true
                } else {
                    Toast.makeText(this, "Invalid Trolley Asset No.. Try Again", Toast.LENGTH_LONG).show()
                }
            }
        }catch (e: Exception){
            Log.e("TAg",e.message!!)
        }
    }


    fun getConnection(user: String, password: String, database: String, server: String): Connection? {
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        var connection: Connection? = null
        var connectionURL: String? = null
        try {
            Class.forName("net.sourceforge.jtds.jdbc.Driver")
            connectionURL = "jdbc:jtds:sqlserver://$server/$database;user=$user;password=$password;"
            connection = DriverManager.getConnection(connectionURL)
        } catch (e: Exception) {
            Log.e("SQL Connection Error : ", e.message!!)
        }
        return connection
    }

    private fun playSound(resId: Int) {
        val mp = MediaPlayer.create(this, resId)
        mp.setOnCompletionListener { mediaPlayer ->
            mediaPlayer.reset()
            mediaPlayer.release()
        }
        mp.start()
    }

    private fun showDialog() {
        val dialog = Dialog(this@LineTrolleyScaningActivity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.custom_input_layout)

        val ref_next = findViewById<Button>(R.id.refNext_btn)

        ref_next.setOnClickListener {
            val edit_refNo = dialog.findViewById<EditText>(R.id.refNo_edit)
            if (edit_refNo.text.isEmpty()){
                edit_refNo.setError("Please Enter a Ref No")
            }else{
                InputBoxValue = edit_refNo.text.toString()
                dialog.dismiss()
            }
        }
        dialog.show()

    }
}