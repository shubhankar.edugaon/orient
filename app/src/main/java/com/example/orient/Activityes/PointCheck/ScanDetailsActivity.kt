package com.example.orient.Activityes.PointCheck

import android.app.Dialog
import android.content.Context
import android.content.SharedPreferences
import android.media.MediaPlayer
import android.os.Bundle
import android.os.StrictMode
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.Window
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.orient.CheckPointAdapter.SeatCheckPointAdapter
import com.example.orient.CheckPointModel.DataModel
import com.example.orient.CheckPointModel.SeatDataModel
import com.example.orient.R
import com.example.orient.SessionManager
import kotlinx.android.synthetic.main.activity_scan_details.firstLayout
import kotlinx.android.synthetic.main.activity_scan_details.labelHeading
import kotlinx.android.synthetic.main.activity_scan_details.lblJobNo_txt
import kotlinx.android.synthetic.main.activity_scan_details.lblPStatus
import kotlinx.android.synthetic.main.activity_scan_details.lblPartName_txt
import kotlinx.android.synthetic.main.activity_scan_details.lblPartcode_txt
import kotlinx.android.synthetic.main.activity_scan_details.lblPosted_txt
import kotlinx.android.synthetic.main.activity_scan_details.lblTQty_txt
import kotlinx.android.synthetic.main.activity_scan_details.listlayou
import kotlinx.android.synthetic.main.activity_scan_details.seatItemRecyclerview
import kotlinx.android.synthetic.main.activity_scan_details.txtScanItem
import java.sql.Connection
import java.sql.DriverManager

class ScanDetailsActivity : AppCompatActivity(), SeatCheckPointAdapter.OnItemClickListener {

    val USERNAME = "USERNNAME"
    val SERVER_IP = "SERVER_IP"
    val SERVER_PASSWORD = "SERVER_PASSWORD"
    val SERVER_NAME = "SERVERNAME"
    val DATABASE_NAME = "DATABASE"
    val SHARED_PREFS = "shared_prefs"
    val TS_LINENUMBER = "TS_LINENUMBER"
    val PLANTNUMBER = "PLANTNUMBER"
    var con: Connection? = null
    var sharedpreferences: SharedPreferences? = null
    private var sessionManager: SessionManager? = null
    private var seatCheckPointAdapter: SeatCheckPointAdapter? = null
    private var SeatDataModelList = ArrayList<SeatDataModel>()
    private var UNLOADJOB = false
    private var RemoveMode = false
    var SERVER_IP_DA = ""
    var SERVER_PASSWORD_DA = ""
    var SERVER_NAME_DA = ""
    var DATABASE_NAME_DA = ""
    var msg = "";
    var record = ""
    var row = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scan_details)

        sessionManager = SessionManager(this@ScanDetailsActivity)
        sharedpreferences = getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE)
        SERVER_IP_DA = sharedpreferences?.getString(SERVER_IP,"").toString()
        SERVER_PASSWORD_DA = sharedpreferences?.getString(SERVER_PASSWORD,"").toString()
        SERVER_NAME_DA = sharedpreferences?.getString(SERVER_NAME,"").toString()
        DATABASE_NAME_DA = sharedpreferences?.getString(DATABASE_NAME,"").toString()

        lblJobNo_txt.text = DataModel.lblJobNo
        lblPartcode_txt.text =DataModel.lblPartcode
        lblPartName_txt.text = DataModel.lblPartName
        lblTQty_txt.text = DataModel.targetQty
        lblPosted_txt.text = DataModel.packedQty

        if (DataModel.targetQty.equals(DataModel.packedQty)){
            AlertDialog("Trolley Loading Completed..")
        }else{
            loadJobOrder()
        }

            loadPlanStatus()


        txtScanItem.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_ENTER && event.action == KeyEvent.ACTION_UP) {
                val data = txtScanItem.text.toString()
                if (data.equals(".1")) {
                    LoadGeneratedNos("")
                } else if (data.equals(".2") && lblPosted_txt.text.toString().length > 0) {
                    firstLayout.setBackgroundColor(resources.getColor(R.color.orangeRed))
                    labelHeading.text = "Scan Seat To Remove"
                    RemoveMode = true
              }
             else if (data.equals(".3")) {
                    LoadGeneratedNos(DataModel.lblJobNo.toString())
                }
                else if (data.equals("..")) {
                    labelHeading.text = "Scan OK Seat Only >"
                    RemoveMode = false
                } else {
                }
//                val list = data.split(";")
//                if (list.size > 1) {
//                    if (list[0].toUpperCase() == DataModel.partCode?.toUpperCase()) {
//                        txtScanItem.setText(list[1].toUpperCase())
//                    } else {
//                        playSound(R.raw.error)
//                        lastScanId.text = "Part Code Not Matching " + list[1]
//
//                    }
//                } else {
//                    txtScanItem.setText(list[0].toUpperCase())
//
//                }
//
//                if (RemoveMode == false) {
//                    saveData(data)
//                } else {
//                    RemoveSeat(data)
//                }
//
//                if (msg.equals("OK")) {
//                    playSound(R.raw.error)
//                    lastScanId.text = "Err: " + lastScanId.text + " " + msg
//                } else {
//                    playSound(R.raw.ok)
//                    lableLastGenerate_number.text = data
//                    if (RemoveMode.equals(true)) {
//                        txtScanItem.setText("Removed: " + data + "")
//                        if (lblPosted_txt.text.toString() <= 0.toString()) {
//                            labelHeading.text = "Scan OK Seat Only >"
//                            RemoveMode = false
//                        } else {
//
//                        }
//                    } else {
//                        txtScanItem.setText("Saved: " + data + "")
//                    }
//                }
//                if (lblPosted_txt.text.toString() >= lblTQty_txt.text.toString()) {
//                    packingCompleteDialog("Trolley Loading Completed..")
//                }

                txtScanItem.getText().clear()
                txtScanItem.setFocusable(true)
                txtScanItem.requestFocus()
                return@OnKeyListener true
            }
                //Perform Code
            false
        })
    }

    private fun RemoveSeat(packetNo:String){
        try {
            con = getConnection(SERVER_NAME_DA ,SERVER_PASSWORD_DA, DATABASE_NAME_DA, SERVER_IP_DA)
            if (con == null) {
                Toast.makeText(this, "Server Link Fail Try Again ....", Toast.LENGTH_LONG).show()
            } else {
                val rowCount = "Select * from tabBabyBoxPrintLog where BabyBoxNo='"+packetNo+"'"
                val smt = con!!.createStatement()
                val resultSet = smt.executeQuery(rowCount)
                var row = 0
                while (resultSet.next()){
                    row++
                    if (row <= 0){
                        msg = "Invalid Seat No. Barcode Scanned..."
                    }else{
                        msg = "OK"
                        if (resultSet.getString("MasterBoxNo").toString().length > 0){
                            if (resultSet.getString("MasterBoxNo").toString().toUpperCase() == DataModel.lblJobNo?.toUpperCase()){
                               val up = "UPDATE tabBabyBoxPrintLog SET MasterBoxNo='' where BabyBoxNo='"+packetNo+"'"
                                smt.execute(up)
                                Integer.parseInt(lblPosted_txt.text.toString())== Integer.parseInt(DataModel.targetQty)-1
                                msg = "OK"
                            }else{
                                playSound(R.raw.error)
                                msg = "Not allowed..."
                            }
                        }else{
                           msg = "Invalid Seat No. Barcode Plan Mismatch..."
                        }
                    }
                }
            }

        }catch (e: Exception){
            Log.e("TAg",e.message!!)
        }
    }

    private fun saveData(packetNo:String){
        try {
            con = getConnection(SERVER_NAME_DA ,SERVER_PASSWORD_DA, DATABASE_NAME_DA, SERVER_IP_DA)
            if (con == null) {
                Toast.makeText(this, "Server Link Fail Try Again ....", Toast.LENGTH_LONG).show()
            } else {
                val rowCount = "Select * from tabBabyBoxPrintLog  where BabyBoxNo='"+packetNo+"'"
                val smt = con!!.createStatement()
                val resultSet = smt.executeQuery(rowCount)
                var row = 0
                while (resultSet.next()){
                    row++
                    if (row <= 0){
                        msg = "Invalid Seat No. Barcode Scanned..."
                    }else{
                        msg = "OK"
                        if (resultSet.getString("MasterBoxNo").toString().length > 0){
                            if (resultSet.getString("MasterBoxNo").toString().toUpperCase() == DataModel.lblJobNo?.toUpperCase()){
                                playSound(R.raw.error)
                                msg = "Seat No. Already Scanned..."
                            }else{
                                playSound(R.raw.error)
                                msg = "Linked On Another Trolly. "+resultSet.getString("MasterBoxNo").toString().toUpperCase()
                            }
                        }else{
                            if (DataModel.PPlanNo.equals(resultSet.getString("ProdPlanNo").toString()) || DataModel.PSeq.equals(resultSet.getString("ProdPlanSeqNo").toString())){
                                    if (DataModel.packedQty.toString() >= DataModel.targetQty.toString()){
                                        playSound(R.raw.error)
                                        msg = "Not Allow Trolly Loading Completed.."
                                    }else{
                                        val update = "UPDATE tabBabyBoxPrintLog SET MasterBoxNo='"+lblJobNo_txt.text.toString()+"' where BabyBoxNo='"+packetNo+"'"
                                        val smt = con!!.createStatement()
                                            smt.execute(update)
                                        lblPosted_txt.text.toString() == DataModel.targetQty+1
                                        msg = "OK"
                                    }
                            }else{
                                playSound(R.raw.error)
                                msg = "Invalid Seat No. Barcode Plan Mismatch..."
                            }
                        }
                    }
                }
            }

        }catch (e: Exception){
            Log.e("TAg",e.message!!)
        }

    }

    private fun loadJobOrder(){
        try {
            con = getConnection(SERVER_NAME_DA ,SERVER_PASSWORD_DA, DATABASE_NAME_DA, SERVER_IP_DA)
            if (con == null) {
                Toast.makeText(this, "Server Link Fail Try Again ....", Toast.LENGTH_LONG).show()
            } else {
                val rowCount = "select tabLineJobLog.*,tabItem.ItemDescription from tabLineJobLog,tabItem where tabLineJobLog.ItemCode=tabItem.ItemCode and  closed=0 and PlantNo='"+sharedpreferences?.getString(PLANTNUMBER,"").toString()+"' and ProdLineNo='"+sharedpreferences?.getString(TS_LINENUMBER,"").toString()+"'"
                val smt = con!!.createStatement()
                val resultSet = smt.executeQuery(rowCount)
                if (resultSet.next()){
                    val JobNo = resultSet.getString("JobID")
                    if (JobNo > 0.toString()){
                        UNLOADJOB()
                    }else{
                       val b = DataModel.packedQty?.toInt()?.minus(DataModel.targetQty?.toInt()!!)
                        val rowCount = "SP_START_JOB_SCHEDULE '"+sharedpreferences?.getString(PLANTNUMBER,"").toString()+"', '"+DataModel.PPlanNo+"','"+DataModel.PSeq+"','"+sharedpreferences?.getString(TS_LINENUMBER,"").toString()+"', '"+DataModel.shift+"','"+sharedpreferences?.getString(USERNAME,"").toString()+"','"+DataModel.lblPartcode+"','"+"-"+b.toString()+"', '"+sharedpreferences?.getString(USERNAME,"").toString()+"'"
                        val resultSet = smt.executeQuery(rowCount)
                            if (resultSet.next()){
                               val JobNo = resultSet.getString("JobID")
                                DataModel.JobNo = JobNo
                                Toast.makeText(this,"Job No.:"+JobNo+" Started...",Toast.LENGTH_LONG).show()
                            }else{
                                Toast.makeText(this,"Job Set Fail..",Toast.LENGTH_LONG).show()
                            }
                    }
                }
            }
        }catch (e: Exception){
            Log.e("TAg",e.message!!)
        }

    }

    private fun UNLOADJOB(){
        try {
            con = getConnection(SERVER_NAME_DA ,SERVER_PASSWORD_DA, DATABASE_NAME_DA, SERVER_IP_DA)
            if (con == null) {
                Toast.makeText(this, "Server Link Fail Try Again ....", Toast.LENGTH_LONG).show()
            } else {
                val rowCount = "SP_END_JOB_SCHEDULE '"+DataModel.JobNo+"','"+sharedpreferences?.getString(USERNAME,"").toString()+"',0"
                val smt = con!!.createStatement()
                val resultSet = smt.executeQuery(rowCount)
                if (resultSet.next()){
                    UNLOADJOB = true
                }else{
                    UNLOADJOB = false
                }
            }

        }catch (e: Exception){
                    Log.e("TAg",e.message!!)
                }

    }

    private fun LoadGeneratedNos(TrollyNo:String){
        try {
            con = getConnection(SERVER_NAME_DA ,SERVER_PASSWORD_DA, DATABASE_NAME_DA, SERVER_IP_DA)
            if (con == null) {
                Toast.makeText(this, "Server Link Fail Try Again ....", Toast.LENGTH_LONG).show()
            } else {
                var rowCount = ""
                if (TrollyNo==""){
                     rowCount = "Select BabyBoxNo,MasterBoxNo from tabBabyBoxPrintLog  where ProdPlanNo='"+DataModel.PPlanNo+"' and ProdPlanSeqNo='"+DataModel.PSeq+"' and ProdLineNo='"+sharedpreferences?.getString(TS_LINENUMBER,"").toString()+"'"
                    }else{
                      rowCount = "Select BabyBoxNo,MasterBoxNo from tabBabyBoxPrintLog  where MasterBoxNo ='"+TrollyNo+"' and ProdPlanNo='"+DataModel.PPlanNo+"' and ProdPlanSeqNo='"+DataModel.PSeq+"' and ProdLineNo='"+sharedpreferences?.getString(TS_LINENUMBER,"").toString()+"'"
                }
                val smt = con!!.createStatement()
                val resultSet = smt.executeQuery(rowCount)
                while (resultSet.next()){
                        val BabyBoxNo = resultSet.getString("BabyBoxNo")
                        val MasterBoxNo = resultSet.getString("MasterBoxNo")
                    val seatDataModel = SeatDataModel(BabyBoxNo,MasterBoxNo)
                    if (MasterBoxNo.length > 0){
                        seatItemRecyclerview.setBackgroundColor(resources.getColor(R.color.green_700))
                    }
                    firstLayout.visibility = View.GONE
                    listlayou.visibility = View.VISIBLE
                    SeatDataModelList.add(seatDataModel)
                    showOldList(SeatDataModelList)
                }
               // Toast.makeText(this,"Not Found",Toast.LENGTH_LONG).show()

            }

        }catch (e: Exception){
            Log.e("TAg",e.message!!)
        }
    }

    private fun loadPlanStatus(){
        try {
            con = getConnection(SERVER_NAME_DA ,SERVER_PASSWORD_DA, DATABASE_NAME_DA, SERVER_IP_DA)
            if (con == null) {
                Toast.makeText(this, "Server Link Fail Try Again ....", Toast.LENGTH_LONG).show()
            } else {
                val rowCount = "Select * from tabProdPlanDetail  where ProdPlanNo='"+DataModel.PPlanNo+"' and SeqNo='"+DataModel.PSeq+"'"
                val smt = con!!.createStatement()
                val resultSet = smt.executeQuery(rowCount)
                if (resultSet.next()){
                   DataModel.targetQty = resultSet.getString("QTY")
                    val selectQ = "Select * from tabBabyBoxPrintLog  where LEN( ISNULL(MasterBoxNo,''))>0 AND ProdPlanNo='"+DataModel.PPlanNo+"' and ProdPlanSeqNo='"+DataModel.PSeq+"'"
                    val data = smt.executeQuery(selectQ)
                    while (data.next()){
                        val ProdPlanNo = data.getString("ProdPlanNo").toString()
                        DataModel.targetQty = row++.toString()
                    }
                    lblPStatus.text = DataModel.packedQty+" / "+DataModel.targetQty
                }else{

                }
            }

        }catch (e: Exception){
            Log.e("TAg",e.message!!)
        }

    }

    private fun showOldList(SeatDataModel: List<SeatDataModel>){
        seatCheckPointAdapter = SeatCheckPointAdapter(SeatDataModel,this)
        val layoutManage = LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false)
        seatItemRecyclerview.layoutManager = layoutManage
        seatItemRecyclerview.adapter = seatCheckPointAdapter
    }

    private fun AlertDialog(title:String){
        val dialog = Dialog(this@ScanDetailsActivity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.custom_layout)
        val body = dialog.findViewById(R.id.title_txt) as TextView
        body.text = title
        val yesBtn = dialog.findViewById(R.id.ok_btn) as Button
        yesBtn.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }

    fun getConnection(user: String, password: String, database: String, server: String): Connection? {
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        var connection: Connection? = null
        var connectionURL: String? = null
        try {
            Class.forName("net.sourceforge.jtds.jdbc.Driver")
            connectionURL = "jdbc:jtds:sqlserver://$server/$database;user=$user;password=$password;"
            connection = DriverManager.getConnection(connectionURL)
        } catch (e: Exception) {
            Log.e("SQL Connection Error : ", e.message!!)
        }
        return connection
    }

    override fun onItemClick(seatDataModel: SeatDataModel) {
        PrintBoxLabel(seatDataModel.BabyBoxNo.toString())

    }

    private fun PrintBoxLabel(TraceNo:String){
        try {
            con = getConnection(SERVER_NAME_DA ,SERVER_PASSWORD_DA, DATABASE_NAME_DA, SERVER_IP_DA)
            if (con == null) {
                Toast.makeText(this, "Server Link Fail Try Again ....", Toast.LENGTH_LONG).show()
            } else {
                val query = "Select * from tabLinePrinterDTL  where ProdLineNo='"+sharedpreferences?.getString(TS_LINENUMBER,"").toString()+"' and  PlantNo='"+sharedpreferences?.getString(PLANTNUMBER,"".toString())+"'"
                val smt = con!!.createStatement()
                val data = smt.executeQuery(query)
                if (data.next()){
                    val PrinterIP = data.getString("PrinterIP")
                    val PrinterPort = data.getString("PrinterPort")
                    var pclFormate = ""
                    val qu = "Select * from tabItem  where ItemCode='"+lblPartcode_txt.text.toString()+"'"
                    val data1 = smt.executeQuery(qu)
                    if (data1.next()){
                        pclFormate = data1.getString("BabyLabelFormat").toString()
                    }else{
                        val addQuery = "insert into tabPrintQ (EntryTime,TraceNo,PrinterIP,PrinterPort,FormatFile,MasterBox) values(getdate(),'"+TraceNo+"','"+PrinterIP+"','"+PrinterPort+"','"+pclFormate+"','B'"
                            val data2 = smt.executeQuery(addQuery)
                        if (data2.next()){
                            AlertDialog("Re-Printed...")
                        }else{
                            AlertDialog("Err: Reprint Fail...")
                            playSound(R.raw.error)
                        }
                    }
                }else{
                    playSound(R.raw.error)
                    AlertDialog("Err: Reprint Fail...")
                }
            }
        }catch (e:Exception){

        }
    }
    private fun playSound(resId: Int) {
        val mp = MediaPlayer.create(this, resId)
        mp.setOnCompletionListener { mediaPlayer ->
            mediaPlayer.reset()
            mediaPlayer.release()
        }
        mp.start()
    }

}