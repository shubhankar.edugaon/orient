package com.example.orient.Activityes.PointCheck

import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.StrictMode
import android.util.Log
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.orient.CheckPointAdapter.SecurityCheckPointAdapter
import com.example.orient.CheckPointModel.SecurityCheckModel
import com.example.orient.R
import com.example.orient.SessionManager
import kotlinx.android.synthetic.main.activity_security_check.securityCheck_recyclerview
import kotlinx.android.synthetic.main.activity_security_check.security_img
import java.sql.Connection
import java.sql.DriverManager
import java.util.ArrayList

class SecurityCheckActivity : AppCompatActivity(),SecurityCheckPointAdapter.OnItemClickListener {
    val USERNAME = "USERNNAME"
    val SERVER_IP = "SERVER_IP"
    val SERVER_PASSWORD = "SERVER_PASSWORD"
    val SERVER_NAME = "SERVERNAME"
    val DATABASE_NAME = "DATABASE"

    val SHARED_PREFS = "shared_prefs"
    var con: Connection? = null
    var sharedpreferences: SharedPreferences? = null
    private var sessionManager: SessionManager? = null
    private var SecurityCheckPoint = ArrayList<SecurityCheckModel>()
    lateinit var securityCheckPointAdapter: SecurityCheckPointAdapter
    var SERVER_IP_DA = ""
    var SERVER_PASSWORD_DA = ""
    var SERVER_NAME_DA = ""
    var DATABASE_NAME_DA = ""
    var packSize=0
    var clickCount = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_security_check)

        security_img.setOnClickListener {
            onBackPressed()
        }

        sessionManager = SessionManager(this@SecurityCheckActivity)
        sharedpreferences = getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE)
        SERVER_IP_DA = sharedpreferences?.getString(SERVER_IP,"").toString()
        SERVER_PASSWORD_DA = sharedpreferences?.getString(SERVER_PASSWORD,"").toString()
        SERVER_NAME_DA = sharedpreferences?.getString(SERVER_NAME,"").toString()
        DATABASE_NAME_DA = sharedpreferences?.getString(DATABASE_NAME,"").toString()

        getOldBox()
    }


    private fun getOldBox(){
        try {
            con = connectionClass(SERVER_NAME_DA ,SERVER_PASSWORD_DA, DATABASE_NAME_DA, SERVER_IP_DA)
            if (con == null) {
                Toast.makeText(this, "Server Link Fail Try Again ....", Toast.LENGTH_LONG).show()
            } else {
                val sql = "SELECT * FROM tabVerifyASNLog"
                val smt = con!!.createStatement()
                val data = smt.executeQuery(sql)
                var row  = data.row
                SecurityCheckPoint.clear()
                while (data.next()) {
                    row=row+1
                    var b = data.row
                    val EntryNo = data.getString("EntryNo")
                    val OrderNo = data.getString("OrderNo")
                    val CompleteTime = data.getString("CompleteTime")
                    val SecurityCheckedBy = data.getString("SecurityCheckedBy")
                    val SecurityCheckedOn = data.getString("SecurityCheckedOn")
                    val securityCheckModel = SecurityCheckModel(OrderNo,OrderNo,EntryNo,OrderNo,CompleteTime,SecurityCheckedOn,SecurityCheckedBy,EntryNo)
                    SecurityCheckPoint.add(securityCheckModel)

                    showOldList(SecurityCheckPoint)
                }
            }

        }catch (e: Exception){
            Log.e("TAg",e.message!!)
        }
    }

    private fun showOldList(SecurityCheckPoint: List<SecurityCheckModel>){
        securityCheckPointAdapter = SecurityCheckPointAdapter(SecurityCheckPoint,this)
        val layoutManage = LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false)
        securityCheck_recyclerview.layoutManager = layoutManage
        securityCheck_recyclerview.adapter = securityCheckPointAdapter
    }

    fun connectionClass(user: String, password: String, database: String, server: String): Connection? {
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        var connection: Connection? = null
        var connectionURL: String? = null
        try {
            Class.forName("net.sourceforge.jtds.jdbc.Driver")
            connectionURL = "jdbc:jtds:sqlserver://$server/$database;user=$user;password=$password;"
            connection = DriverManager.getConnection(connectionURL)
        } catch (e: Exception) {
            Log.e("SQL Connection Error : ", e.message!!)
        }
        return connection
    }

    override fun onItemClick(securityCheckModel: SecurityCheckModel) {

    }

}