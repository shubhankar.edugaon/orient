package com.example.orient.Activityes.PointCheck

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.media.MediaPlayer
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.StrictMode
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.core.widget.doAfterTextChanged
import com.example.orient.CheckPointModel.DataModel
import com.example.orient.R
import com.example.orient.SessionManager
import kotlinx.android.synthetic.main.activity_box_paking.boxScan_editText
import kotlinx.android.synthetic.main.activity_select_shift.masterBarcode_layout
import kotlinx.android.synthetic.main.activity_select_shift.scan_master_edit
import kotlinx.android.synthetic.main.activity_select_shift.shift_next_btn
import kotlinx.android.synthetic.main.activity_select_shift.shift_txt
import kotlinx.android.synthetic.main.activity_select_shift.spinner
import kotlinx.android.synthetic.main.activity_select_shift.spinner_layout
import java.sql.Connection
import java.sql.DriverManager

class SelectShiftActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {

    val USERNAME = "USERNNAME"
    val SERVER_IP = "SERVER_IP"
    val SERVER_PASSWORD = "SERVER_PASSWORD"
    val SERVER_NAME = "SERVERNAME"
    val DATABASE_NAME = "DATABASE"
    val SHARED_PREFS = "shared_prefs"
    var con: Connection? = null
    var sharedpreferences: SharedPreferences? = null
    private var sessionManager: SessionManager? = null
    var arrayAdapter: ArrayAdapter<String>? = null
    var SERVER_IP_DA = ""
    var SERVER_PASSWORD_DA = ""
    private var checked = false
    var SERVER_NAME_DA = ""
    var DATABASE_NAME_DA = ""

    var shift = arrayOf("A", "B", "C")
     var shiftValue = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_shift)

        sessionManager = SessionManager(this@SelectShiftActivity)
        sharedpreferences = getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE)
        SERVER_IP_DA = sharedpreferences?.getString(SERVER_IP,"").toString()
        SERVER_PASSWORD_DA = sharedpreferences?.getString(SERVER_PASSWORD,"").toString()
        SERVER_NAME_DA = sharedpreferences?.getString(SERVER_NAME,"").toString()
        DATABASE_NAME_DA = sharedpreferences?.getString(DATABASE_NAME,"").toString()


        spinner!!.setOnItemSelectedListener(this)
        val aa = ArrayAdapter(this, android.R.layout.simple_spinner_item, shift)
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner!!.setAdapter(aa)

        //getCount()

        shift_next_btn.setOnClickListener {
            spinner_layout.visibility = View.GONE
            masterBarcode_layout.visibility = View.VISIBLE

        }
        scan_master_edit.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_ENTER && event.action == KeyEvent.ACTION_UP) {
                val data = scan_master_edit.text.toString()
                if (data.length > 0){
                    GetValidateData(data)
                }
                //Perform Code
                scan_master_edit.text.clear()
                scan_master_edit.clearFocus()
                return@OnKeyListener true
            }
            false
        })

    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        shift_txt.text = shift[position]
        shiftValue = shift[position]
        DataModel.shift = shiftValue
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
    }

    private fun GetValidateData(masterCode:String){
        try {
            con = getConnection(SERVER_NAME_DA ,SERVER_PASSWORD_DA, DATABASE_NAME_DA, SERVER_IP_DA)
            if (con == null) {
                Toast.makeText(this, "Server Link Fail Try Again ....", Toast.LENGTH_LONG).show()
            } else {
                val sql = "Select * from tabItem where ItemCode='"+masterCode+"'"
                val smt = con!!.createStatement()
                val data = smt.executeQuery(sql)
                if (data.next()) {
                        checked = true
                   val intent = Intent(this,LineTrolleyScaningActivity::class.java)
                    DataModel.partCode = data.getString("ItemCode")
                    startActivity(intent)

                } else {
                    Toast.makeText(this, "Invalid Partcode.. Try Again", Toast.LENGTH_LONG).show()
                    playSound(R.raw.error)
                }
            }
        }catch (e: Exception){
            playSound(R.raw.error)
            Toast.makeText(this,"Server Link Fail Try Again ....", Toast.LENGTH_LONG).show()
        }
    }

    private fun getCount(){
        try {
            con = getConnection(SERVER_NAME_DA ,SERVER_PASSWORD_DA, DATABASE_NAME_DA, SERVER_IP_DA)
            if (con == null) {
                Toast.makeText(this, "Server Link Fail Try Again ....", Toast.LENGTH_LONG).show()
            } else {
            val rowCount = "Select count(*) as data from tabBabyBoxPrintLog where MasterBoxNo='0102180248'"
            val smt = con!!.createStatement()
            val resultSet = smt.executeQuery(rowCount)
            if (resultSet.next()){
                val rowCount = resultSet.getString("data")
                Toast.makeText(this,rowCount,Toast.LENGTH_LONG).show()
            }
            }
        }catch (e: Exception){
            Log.e("TAg",e.message!!)
        }

    }
    private fun playSound(resId: Int) {
        val mp = MediaPlayer.create(this, resId)
        mp.setOnCompletionListener { mediaPlayer ->
            mediaPlayer.reset()
            mediaPlayer.release()
        }
        mp.start()
    }

    fun getConnection(user: String, password: String, database: String, server: String): Connection? {
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        var connection: Connection? = null
        var connectionURL: String? = null
        try {
            Class.forName("net.sourceforge.jtds.jdbc.Driver")
            connectionURL = "jdbc:jtds:sqlserver://$server/$database;user=$user;password=$password;"
            connection = DriverManager.getConnection(connectionURL)
        } catch (e: Exception) {
            Log.e("SQL Connection Error : ", e.message!!)
        }
        return connection
    }
}