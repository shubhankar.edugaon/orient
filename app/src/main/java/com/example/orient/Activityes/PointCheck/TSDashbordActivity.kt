package com.example.orient.Activityes.PointCheck

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.orient.Activityes.Orient.LoginActivity
import com.example.orient.Activityes.Orient.SeetingsActivity
import com.example.orient.R
import com.example.orient.SessionManager
import kotlinx.android.synthetic.main.activity_dashbord2.dispatch_cardView
import kotlinx.android.synthetic.main.activity_dashbord2.production_cardview
import kotlinx.android.synthetic.main.activity_dashbord2.securityCheck_cardView
import kotlinx.android.synthetic.main.activity_dashbord2.setting_cardView
import java.sql.Connection

class TSDashbordActivity : AppCompatActivity() {
    val SHARED_PREFS = "shared_prefs"
    var sharedpreferences: SharedPreferences? = null
    private var sessionManager: SessionManager? = null
    var con: Connection? = null
    val IS_USER_LOGIN = "IsUserLoggedIn"
    val USERNAME = "USERNNAME"
    val SERVER_IP = "SERVER_IP"
    val SERVER_PASSWORD = "SERVER_PASSWORD"
    val SERVER_NAME = "SERVERNAME"
    val DATABASE_NAME = "DATABASE"
    val SERVER_STATUS = "CONNECT"

    var SERVER_IP_DA = ""
    var SERVER_PASSWORD_DA = ""
    var SERVER_NAME_DA = ""
    var DATABASE_NAME_DA = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashbord2)

        sessionManager = SessionManager(this@TSDashbordActivity)
        sharedpreferences = getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE)
        SERVER_IP_DA = sharedpreferences?.getString(SERVER_IP,"").toString()
        SERVER_PASSWORD_DA = sharedpreferences?.getString(SERVER_PASSWORD,"").toString()
        SERVER_NAME_DA = sharedpreferences?.getString(SERVER_NAME,"").toString()
        DATABASE_NAME_DA = sharedpreferences?.getString(DATABASE_NAME,"").toString()
        if (sharedpreferences?.getString(USERNAME,null)!=null){
            Toast.makeText(this,"User is Logined"+sharedpreferences?.getString(USERNAME,"").toString(), Toast.LENGTH_LONG).show()
        }else{
            startActivity(Intent(this, LoginActivity::class.java))
        }

        setting_cardView.setOnClickListener{
            startActivity(Intent(this,SeetingsActivity::class.java))
        }

        production_cardview.setOnClickListener {
            startActivity(Intent(this, SelectShiftActivity::class.java))
        }

        dispatch_cardView.setOnClickListener {
            startActivity(Intent(this, TSDispatchActivity::class.java))
        }

        securityCheck_cardView.setOnClickListener {
            startActivity(Intent(this, SecurityCheckActivity::class.java))
        }
    }
}