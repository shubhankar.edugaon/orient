package com.example.orient.Activityes.PointCheck

import android.content.Context
import android.content.SharedPreferences
import android.media.MediaPlayer
import android.os.Bundle
import android.os.StrictMode
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doAfterTextChanged
import com.example.orient.CheckPointModel.FrmPicKListModel
import com.example.orient.R
import com.example.orient.SessionManager
import kotlinx.android.synthetic.main.activity_tsdispatch.scanData
import kotlinx.android.synthetic.main.activity_tsdispatch.status_click
import kotlinx.android.synthetic.main.activity_tsdispatch.status_layout
import kotlinx.android.synthetic.main.activity_tsdispatch.trolleyDetails
import kotlinx.android.synthetic.main.activity_tsdispatch.trolley_layout
import kotlinx.android.synthetic.main.activity_tsdispatch.tsTechScan_master
import java.sql.Connection
import java.sql.DriverManager
import java.sql.ResultSet

class TSDispatchActivity : AppCompatActivity() {

    val USERNAME = "USERNNAME"
    val SERVER_IP = "SERVER_IP"
    val SERVER_PASSWORD = "SERVER_PASSWORD"
    val SERVER_NAME = "SERVERNAME"
    val DATABASE_NAME = "DATABASE"
    val SHARED_PREFS = "shared_prefs"
    var con: Connection? = null
    var sharedpreferences: SharedPreferences? = null
    private var sessionManager: SessionManager? = null
    var arrayAdapter: ArrayAdapter<String>? = null
    var SERVER_IP_DA = ""
    var SERVER_PASSWORD_DA = ""
    var SERVER_NAME_DA = ""
    var DATABASE_NAME_DA = ""
    var caseDta = ""
    var txtSlipNo = ""
    var checkPickListSlip = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tsdispatch)

        sessionManager = SessionManager(this@TSDispatchActivity)
        sharedpreferences = getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE)
        SERVER_IP_DA = sharedpreferences?.getString(SERVER_IP,"").toString()
        SERVER_PASSWORD_DA = sharedpreferences?.getString(SERVER_PASSWORD,"").toString()
        SERVER_NAME_DA = sharedpreferences?.getString(SERVER_NAME,"").toString()
        DATABASE_NAME_DA = sharedpreferences?.getString(DATABASE_NAME,"").toString()


        trolleyDetails.setOnClickListener {
            trolley_layout.visibility = View.VISIBLE
            status_layout.visibility = View.GONE
        }

        status_click.setOnClickListener {
            trolley_layout.visibility = View.GONE
            status_layout.visibility = View.VISIBLE

        }

        tsTechScan_master.doAfterTextChanged {
                val data = tsTechScan_master.text.toString()
                if (data.length > 0){
                    scanData.text = data
                    checkPickListSlip(data)
                    if (caseDta.equals("p")){

                    }
                }
        }


    }

    private fun checkPickListSlip(slipNo:String){
        checkPickListSlip = "E"
                try {
                    con = connectionClass(SERVER_NAME_DA ,SERVER_PASSWORD_DA, DATABASE_NAME_DA, SERVER_IP_DA)
                    if (con == null) {
                            val checkPickQ = "Select * from VW_LOAD_PLAN where OrderNo = '"+slipNo+"'"
                            val smt = con!!.createStatement()
                            val data: ResultSet = smt.executeQuery(checkPickQ)
                                if (data.next()){
                                    FrmPicKListModel.SlipStatus = data.getString("OrderNo")
                                    FrmPicKListModel.TXTTRUCKNO = data.getString("TruckNo")
                                    FrmPicKListModel.planQty = data.getString("TOrderQty")
                                    FrmPicKListModel.loadQty = data.getString("TNooftrolly")
                                    FrmPicKListModel.SlipStatus = data.getString("Status")
                                    FrmPicKListModel.txtASNNo = data.getString("OrderNo")
                                    checkPickListSlip = "P"
                                } else{
                                    playSound(R.raw.error)
                                    Toast.makeText(this,"Invalid Loading Plan No...",Toast.LENGTH_LONG).show()
                                }
                    }
                }catch (e:Exception){

                }
    }

    private fun LoadPickListSummary(slipNo:String){
        try {
            con = connectionClass(SERVER_NAME_DA ,SERVER_PASSWORD_DA, DATABASE_NAME_DA, SERVER_IP_DA)
            if (con == null) {
                val checkPickQ = "SELECT  ScheduleID, PartCode, ItemDescription, ItemGroup, ItemRange, ItemColour, OrderQty, NoofTrolly, LoadedTrollyCount, Loaded FROM  VW_LOAD_STATUS where  OrderNo='"+txtSlipNo+"'"
                val smt = con!!.createStatement()
                val data: ResultSet = smt.executeQuery(checkPickQ)
                if (data.next()){

                    checkPickListSlip = "P"
                } else{
                    playSound(R.raw.error)
                    Toast.makeText(this,"Invalid Loading Plan No...",Toast.LENGTH_LONG).show()
                }
            }
        }catch (e:Exception){

        }
    }




    fun connectionClass(user: String, password: String, database: String, server: String): Connection? {
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        var connection: Connection? = null
        var connectionURL: String? = null
        try {
            Class.forName("net.sourceforge.jtds.jdbc.Driver")
            connectionURL = "jdbc:jtds:sqlserver://$server/$database;user=$user;password=$password;"
            connection = DriverManager.getConnection(connectionURL)
        } catch (e: Exception) {
            Log.e("SQL Connection Error : ", e.message!!)
        }
        return connection
    }

    private fun playSound(resId: Int) {
        val mp = MediaPlayer.create(this, resId)
        mp.setOnCompletionListener { mediaPlayer ->
            mediaPlayer.reset()
            mediaPlayer.release()
        }
        mp.start()
    }
}