package com.example.orient.Activityes

import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.orient.R
import com.example.orient.SessionManager
import kotlinx.android.synthetic.main.activity_settings.*

class SettingsActivity : AppCompatActivity() {
    val SHARED_PREFS = "shared_prefs"
    var sharedpreferences: SharedPreferences? = null
    private var sessionManager: SessionManager? = null
    val USERNAME = "USERNNAME"
    val URL = "URL"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)

        sessionManager = SessionManager(this@SettingsActivity)
        sharedpreferences = getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE)

        save_url.setOnClickListener {
            val url = url_edit.text.toString().trim()
            if (url.isEmpty()){
                url_edit.error = "Please enter a url"
            }else{
                sessionManager?.SaveUrl(url_edit.text.toString().trim())
                Toast.makeText(this," URL Saved "+url_edit.text.toString(),Toast.LENGTH_LONG).show()
                saveUrl.text = sharedpreferences?.getString(URL,"").toString()

            }
        }
        arrow_back_img.setOnClickListener {
            onBackPressed()
        }
    }

    override fun onResume() {
        super.onResume()
        val url = sharedpreferences?.getString(URL,"").toString()
        url_edit.setText(url)

    }
}