package com.example.orient.Activityes

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.SharedPreferences
import android.os.Bundle
import android.webkit.WebSettings
import android.webkit.WebViewClient
import android.widget.MediaController
import androidx.appcompat.app.AppCompatActivity
import com.example.orient.R
import com.example.orient.SessionManager
import kotlinx.android.synthetic.main.activity_start_application.*


class StartApplicationActivity : AppCompatActivity() {
    val SHARED_PREFS = "shared_prefs"
    var sharedpreferences: SharedPreferences? = null
    private var sessionManager: SessionManager? = null
    val URL = "URL"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start_application)

        sessionManager = SessionManager(this@StartApplicationActivity)
        sharedpreferences = getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE)

        val url = sharedpreferences?.getString(URL,"").toString()

        webView_url.webViewClient = WebViewClient()
        webView_url.loadUrl(url)
        webView_url.settings.javaScriptEnabled = true
        webView_url.getSettings().setJavaScriptEnabled(true);
        webView_url.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        webView_url.getSettings().setMediaPlaybackRequiresUserGesture(false);
        webView_url.settings.setSupportZoom(true)

        // media controller class
        // media controller class
        val mediaController = MediaController(this)

        // sets the anchor view
        // anchor view for the videoView

        // sets the anchor view
        // anchor view for the videoView
        mediaController.setAnchorView(webView_url)

        // sets the media player to the videoView

        // sets the media player to the videoView
//        mediaController.setMediaPlayer(webView_url)
//
//        // sets the media controller to the videoView
//
//        // sets the media controller to the videoView
//        webView_url.setMediaController(mediaController)

    }
    override fun onBackPressed() {
        AlertDialog.Builder(this)
            .setMessage("Are you sure you want to exit?")
            .setCancelable(false)
            .setPositiveButton("Yes", DialogInterface.OnClickListener {
                    dialog, id -> super@StartApplicationActivity.onBackPressed() })
            .setNegativeButton("No", null)
            .show()
    }

    }

