package com.example.orient.Activityes

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.orient.Activityes.Orient.MainActivity
import com.example.orient.R
import kotlinx.android.synthetic.main.activity_web_view.*

class WebViewActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web_view)

        next_btn.setOnClickListener {
                var  intent = Intent(this, SettingsActivity::class.java)
                startActivity(intent)
            }
        start_btn.setOnClickListener {
            startActivity(Intent(this,StartApplicationActivity::class.java))
        }

        exit_btn.setOnClickListener {
            exitProcess()
        }
        }

    private fun exitProcess() {
        //finish()
        System.exit(-1)
    }

    }
