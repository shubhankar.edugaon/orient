package com.example.orient.Adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.orient.Dispatch.NewItemModel
import com.example.orient.Dispatch.OldItemModel
import com.example.orient.R
import kotlinx.android.synthetic.main.new_box_item.view.*
import kotlinx.android.synthetic.main.old_list_items.view.*


class NewItemAdapter(var newItemList: List<NewItemModel>, private var onItemClickListener: OnItemClickListener): RecyclerView.Adapter<NewItemAdapter.NewItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewItemAdapter.NewItemViewHolder {
        return NewItemViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.new_box_item, parent, false), onItemClickListener)
    }

    override fun onBindViewHolder(holder: NewItemViewHolder, position: Int) {
        holder.bind(newItemList[position])
        val data = newItemList[position]
        holder.newItemList = data
    }

    override fun getItemCount(): Int {
        return newItemList.size
    }

    class NewItemViewHolder(itemView: View, onItemClickListener: OnItemClickListener,): RecyclerView.ViewHolder(itemView){
        lateinit var newItemList: NewItemModel
        fun bind(newItemModel: NewItemModel){
            itemView.item_value.text = newItemModel.SAPPoductCode
            itemView.qty_value.text = newItemModel.PackQty
        }
        init {
            itemView.setOnClickListener {
                onItemClickListener.onItemClick(newItemList)
            }
        }
    }

    interface OnItemClickListener{
        fun onItemClick(newItemModel: NewItemModel)
    }
}
