package com.example.orient.Adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.orient.Dispatch.OldItemModel
import com.example.orient.R
import kotlinx.android.synthetic.main.old_list_items.view.*


class OldItemAdapter(var oldItemList: List<OldItemModel>, private var onItemClickListener: OnItemClickListener): RecyclerView.Adapter<OldItemAdapter.OldItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OldItemAdapter.OldItemViewHolder {
        return OldItemViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.old_list_items, parent, false), onItemClickListener)
    }

    override fun onBindViewHolder(holder: OldItemViewHolder, position: Int) {
        holder.bind(oldItemList[position])
        val data = oldItemList[position]
        holder.oldItemList = data
    }

    override fun getItemCount(): Int {
        //return oldItemList.size
        val limit = 5
        return Math.min(oldItemList.size, limit)
    }

    class OldItemViewHolder(itemView: View, onItemClickListener: OnItemClickListener): RecyclerView.ViewHolder(itemView){
        lateinit var oldItemList: OldItemModel
        fun bind(oldItemModel: OldItemModel){
            itemView.refNo_txt.text = oldItemModel.IssueNo
            itemView.date_txt.text = oldItemModel.DDate
        }
        init {
            itemView.setOnClickListener {
                onItemClickListener.onItemClick(oldItemList)
            }
        }
    }

    interface OnItemClickListener{
        fun onItemClick(oldItemModel: OldItemModel)
    }
}
