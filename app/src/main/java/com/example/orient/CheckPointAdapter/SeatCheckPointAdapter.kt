package com.example.orient.CheckPointAdapter

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.recyclerview.widget.RecyclerView
import com.example.orient.CheckPointModel.SeatDataModel
import com.example.orient.R
import kotlinx.android.synthetic.main.line_item_list.view.seatNo_text
import kotlinx.android.synthetic.main.line_item_list.view.seat_item_layout
import kotlinx.android.synthetic.main.line_item_list.view.trolley_txt

class SeatCheckPointAdapter(var seatCheckItemList: List<SeatDataModel>, private var onItemClickListener: OnItemClickListener): RecyclerView.Adapter<SeatCheckPointAdapter.SeatItemViewHolder>() {
    private var selectedItemPosition: Int = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SeatCheckPointAdapter.SeatItemViewHolder {
        return SeatItemViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.line_item_list, parent, false), onItemClickListener)
    }

    override fun onBindViewHolder(holder: SeatItemViewHolder, position: Int) {
        holder.bind(seatCheckItemList[position])
        val data = seatCheckItemList[position]
        holder.seatItemList = data

        if (selectedItemPosition == position){
            holder.textView.setBackgroundColor(Color.parseColor("#DC746C"))
        }else{

        }
    }

    override fun getItemCount(): Int {
        return seatCheckItemList.size
//        val limit = 5
//        return Math.min(securityCheckItemList.size, limit)
    }


    class SeatItemViewHolder(itemView: View, onItemClickListener: OnItemClickListener): RecyclerView.ViewHolder(itemView){
        lateinit var seatItemList: SeatDataModel
        val textView: LinearLayout = itemView.seat_item_layout
        fun bind(seatDataModel: SeatDataModel){
            itemView.seatNo_text.text = seatDataModel.BabyBoxNo
            itemView.trolley_txt.text = seatDataModel.MasterBoxNo

        }


        init {
            itemView.setOnClickListener {
                onItemClickListener.onItemClick(seatItemList)
            }
        }
    }

    interface OnItemClickListener{
        fun onItemClick(seatDataModel: SeatDataModel)
    }
}
