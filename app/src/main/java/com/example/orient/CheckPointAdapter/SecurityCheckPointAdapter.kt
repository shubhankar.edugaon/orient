package com.example.orient.CheckPointAdapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.orient.CheckPointModel.SecurityCheckModel
import com.example.orient.R
import kotlinx.android.synthetic.main.list_item.view.item_1
import kotlinx.android.synthetic.main.list_item.view.item_2
import kotlinx.android.synthetic.main.list_item.view.item_3
import kotlinx.android.synthetic.main.list_item.view.item_4
import kotlinx.android.synthetic.main.list_item.view.item_5
import kotlinx.android.synthetic.main.list_item.view.item_6
import kotlinx.android.synthetic.main.list_item.view.item_7
import kotlinx.android.synthetic.main.list_item.view.item_8
import kotlinx.android.synthetic.main.old_list_items.view.refNo_txt

class SecurityCheckPointAdapter(var securityCheckItemList: List<SecurityCheckModel>, private var onItemClickListener: OnItemClickListener): RecyclerView.Adapter<SecurityCheckPointAdapter.SecurityCheckItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SecurityCheckPointAdapter.SecurityCheckItemViewHolder {
        return SecurityCheckItemViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_item, parent, false), onItemClickListener)
    }

    override fun onBindViewHolder(holder: SecurityCheckItemViewHolder, position: Int) {
        holder.bind(securityCheckItemList[position])
        val data = securityCheckItemList[position]
        holder.securityItemList = data
    }

    override fun getItemCount(): Int {
        return securityCheckItemList.size
//        val limit = 5
//        return Math.min(securityCheckItemList.size, limit)
    }

    class SecurityCheckItemViewHolder(itemView: View, onItemClickListener: OnItemClickListener): RecyclerView.ViewHolder(itemView){
        lateinit var securityItemList: SecurityCheckModel
        fun bind(securityCheckModel: SecurityCheckModel){
            itemView.item_1.text = securityCheckModel.ASN
            itemView.item_2.text = securityCheckModel.ITEM
            itemView.item_3.text = securityCheckModel.ORDERN
            itemView.item_4.text = securityCheckModel.QTY
            itemView.item_5.text = securityCheckModel.COMPLAINNUMBER
            itemView.item_6.text = securityCheckModel.EntryNo
            itemView.item_7.text = securityCheckModel.SecurityCheckedBy
            itemView.item_8.text = securityCheckModel.SecurityCheckedOn






        }
        init {
            itemView.setOnClickListener {
                onItemClickListener.onItemClick(securityItemList)
            }
        }
    }

    interface OnItemClickListener{
        fun onItemClick(securityCheckModel: SecurityCheckModel)
    }
}
