package com.example.orient.CheckPointModel

data class PickListSummaryModel(
    val ItemDescription:String? = null,
    val OrderQty:String? = null,
    val Loaded:String? = null,
    val ScheduleID:String? = null,
    val PartCode:String? = null,
)