package com.example.orient.CheckPointModel

data class SecurityCheckModel(
    val ASN: String? = null,
    val ITEM: String? = null,
    val QTY: String? = null,
    val ORDERN: String? = null,
    val COMPLAINNUMBER: String? = null,
    val SecurityCheckedBy: String? = null,
    val SecurityCheckedOn: String? = null,
    val EntryNo: String? = null



)