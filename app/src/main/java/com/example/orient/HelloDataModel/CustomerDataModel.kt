package com.example.orient.HelloDataModel

class CustomerDataModel {
}

data class Customer(
    var customerId: String? = null,
    var customName: String? = null
)
data class Invoice(
    var invoiceId:String? = null,
    var invoiceNumber: String? = null
)