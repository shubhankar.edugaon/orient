package com.example.orient.OrientAdapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.orient.OrientDataModel.BoxPackingModel
import com.example.orient.R
import com.example.orient.SessionManager

class PackageAdapter(var boxPackageList: List<BoxPackingModel>, private var onItemClickListener: OnItemClickListener) : RecyclerView.Adapter<PackageAdapter.BoxPackageViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PackageAdapter.BoxPackageViewHolder {
        return BoxPackageViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.box_packing_item_list, parent, false), onItemClickListener)
    }

    override fun onBindViewHolder(holder: PackageAdapter.BoxPackageViewHolder, position: Int) {
        holder.bind(boxPackageList[position])
        val data =boxPackageList[position]
        holder.boxPackageListClickItem = data
    }

    override fun getItemCount(): Int {
        return boxPackageList.size
    }
    class BoxPackageViewHolder(itemView: View, onItemClickListener: OnItemClickListener): RecyclerView.ViewHolder(itemView){
        lateinit var boxPackageListClickItem: BoxPackingModel
        private var sessionManager: SessionManager? = null
        fun bind(boxPackingModel: BoxPackingModel){
            val name = itemView.findViewById<TextView>(R.id.barcode_txt)
            name.text = boxPackingModel.barcode

        }
        init {
            val textViewClickEvent = itemView.findViewById<TextView>(R.id.barcode_txt)
            textViewClickEvent.setOnClickListener {
                onItemClickListener.viewClickEvent(boxPackageListClickItem)
            }
        }
    }
    interface OnItemClickListener{
        fun viewClickEvent(boxPackingModel: BoxPackingModel)
    }


}