package com.example.orient.OrientDataModel

data class ScanCount (
    var totalScan: Int? = null,
    var PrintIp: String? = null,
    var printPort: String? = null
    )

object UserData{
    var userName = String()
}