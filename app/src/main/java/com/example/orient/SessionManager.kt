package com.example.orient

import android.content.Context
import android.content.Intent
import com.example.orient.Activityes.Orient.LoginActivity

class SessionManager(val context: Context) {
    var pref = context.getSharedPreferences("shared_prefs" , Context.MODE_PRIVATE)
    var editor = pref.edit()
    var _context: Context? = null
    var PRIVATE_MODE = 0
    val IS_USER_LOGIN = "IsUserLoggedIn"
    val SERVER_IP = "SERVER_IP"
    val SERVER_PASSWORD = "SERVER_PASSWORD"
    val SERVER_NAME = "SERVERNAME"
    val DATABASE_NAME = "DATABASE"
    val LINENUMBER = "LINENUMBER"
    val PRINTER_IP = "PRINTER_IP"
    val PRINTER_PORT = "PRINTER_PORT"
    val USERNAME = "USERNNAME"
    val PRINTSTATUS = "PRINTSTATUS"
    val SERVER_STATUS = "CONNECT"
    val URL = "URL"
    val PRN = "PRN"
    val WARRANTYPRN = "WARRANTYPRN"
    val PLANTNUMBER = "PLANTNUMBER"
    val TS_LINENUMBER = "TS_LINENUMBER"
    val LOGPASS = "LOGPASS"

    fun SavePrn(prn:String){
        editor?.putString(PRN, prn)
        editor?.commit()
    }

    fun SaveWarrantyPrn(warrantylablePrn: String){
        editor?.putString(WARRANTYPRN, warrantylablePrn)
        editor?.commit()
    }

    fun SaveUrl(url:String){
        editor?.putString(URL, url)
        editor?.commit()
    }

    fun SettingsDetails(serverIp: String?, serverPassword: String?, serverUserName:String?, DatabaseName:String?,lineNumber:String, printer_Ip: String?, printer_port: String,serverStatus: String) {
        editor?.putBoolean(IS_USER_LOGIN, true)
        editor?.putString(SERVER_IP, serverIp)
        editor?.putString(SERVER_PASSWORD, serverPassword)
        editor?.putString(SERVER_NAME, serverUserName)
        editor?.putString(DATABASE_NAME, DatabaseName)
        editor?.putString(SERVER_STATUS, serverStatus)
        editor?.commit()
    }

    fun PrinterDetails(printIp: String?, printPort:String,LineNumber:String,status:String){
        editor?.putString(PRINTER_IP,printIp)
        editor?.putString(PRINTER_PORT,printPort)
        editor?.putString(LINENUMBER,LineNumber)
        editor?.putString(PRINTSTATUS,status)
        editor?.commit()
    }

    fun PlantDetails(plantNu: String?, lineN:String, logPass:String){
        editor.putString(PLANTNUMBER,plantNu)
        editor.putString(TS_LINENUMBER,lineN)
        editor.putString(LOGPASS,logPass)
        editor.commit()
    }

    fun userDetails(userName:String){
        editor.putString(USERNAME,userName)
        editor.commit()

    }

    fun logoutUser(main_context : Context){
        _context= main_context
        editor?.clear()?.apply()
        // editor?.commit()
        val intent = Intent(_context, LoginActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        _context?.startActivity(intent)
    }



}
